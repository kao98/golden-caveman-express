(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
module.exports = class Audio {
  constructor(loader) {
    this.loader = loader;

    this.instances = {
      menuTheme: null,
      mainTheme: null,
      secondTheme: null,
      intro: null,
      move: null,
      cat: null,
      demon: null,
      gameOver: null,
      house: null
    }
  }

  mute() {
    createjs.Sound.muted = true;
  }

  unmute() {
    createjs.Sound.muted = false;
  }

  get muted() {
    return createjs.Sound.muted;
  }

  playMenuTheme() {
    const config = this.createConfig({ loop: -1, volume: 0.5 });

    this.instances.menuTheme = createjs.Sound.play("menu-theme", config);
  }

  stopMenuTheme() {
    createjs.Tween
      .get(this.instances.menuTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.menuTheme.stop());
  }

  playMainTheme() {

    if (this.instances.mainTheme && this.instances.mainTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.mainTheme.paused) {
        this.instances.mainTheme.paused = false;
      }
      return;
    }

    const config = this.createConfig({ startTime: 0, duration: 33000, loop: 0, volume: 0.05 });

    this.instances.mainTheme = createjs.Sound.play("main-theme", config);
    createjs.Tween
      .get(this.instances.mainTheme)
      .to({volume: 0.5}, 500)
      .wait(30500)
      .to({volume: 0}, 2000)
      .call(() => {
        if (this.instances.mainTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
          this.instances.mainTheme.stop();
          this.playMainTheme()
        }
      });
  }

  stopMainTheme() {
    if (!this.instances.mainTheme) {
      return;
    }

    createjs.Tween
      .get(this.instances.mainTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.mainTheme.stop());
  }

  playSecondTheme() {

    if (this.instances.secondTheme && this.instances.secondTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.secondTheme.paused) {
        this.instances.secondTheme.paused = false;
      }
      return;
    }

    const config = this.createConfig({ delay: 5000, startTime: 34000, duration: 31000,loop: -1, volume: 0.05 });

    this.instances.secondTheme = createjs.Sound.play("main-theme", config);
    createjs.Tween
      .get(this.instances.secondTheme)
      .to({volume: 0.5}, 500);

    // this.instances.secondTheme.on('complete', () => {
    //   if (this.instances.secondTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
    //     this.instances.secondTheme.stop();
    //     this.playSecondTheme();
    //   }
    // });
  }

  stopSecondTheme() {
    if (!this.instances.secondTheme) {
      return;
    }

    createjs.Tween
      .get(this.instances.secondTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.secondTheme.stop());
  }

  playMove() {

    if (this.instances.move && this.instances.move.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.move.paused) {
        this.instances.move.paused = false;
      }
      return;
    }

    const config = this.createConfig({ loop: -1, volume: 0.5, startTime: 500, duration: 4000 });

    this.instances.move = createjs.Sound.play("move", config);

  }

  stopMove() {
    this.instances.move.stop();
  }

  playCat() {

    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.cat = createjs.Sound.play("cat-angry", config);

  }

  playDemon() {
    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.demon = createjs.Sound.play("demon", config);
  }

  playGameOver(delay = false) {
    const config = this.createConfig({ delay: !!delay ? 1500 : 0, loop: 0, volume: 0.8 });

    this.instances.gameOver = createjs.Sound.play("game-over", config);
  }

  playInHouse() {
    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.house = createjs.Sound.play("house", config);
  }

  createConfig(options) {
    options = options || {};

    return new createjs.PlayPropsConfig().set({
      interrupt: options.interrupt || createjs.Sound.INTERRUPT_ANY,
      loop: options.loop || 0, //0: no-loop - -1: inifinite
      volume: options.volume || 0.8, //between 0 and 1
      delay: options.delay || 0, //ms
      offset: options.offset || 0, //ms
      pan: options.pan || 0, //between -1 and 1
      startTime: options.startTime || null, //ms - for audio sprites - offset to start playback and loop from
      duration: options.duration || null //ms - for audio sprites
    });
  }
}

},{}],2:[function(require,module,exports){

module.exports = class Enemy {

  constructor(loader, stage, x, y) {
    this.x = x;
    this.y = y;
    this.vec = {x: 0, y: 0};
    this.stage = stage;

    this.speed = 4.5;

    this.startX = x;
    this.startY = y;

    this.dev = false;

    // image
    if (Math.random() < 0.5) {
        // cat --> saute sur le joueur si assez près
        this.type = 0;
        let sh = new createjs.SpriteSheet({
          images: [loader.get("cat")],
          frames: {count: 4, width: 32, height: 32 },
          animations: { idle: [0, 2, undefined, 0.24 ], attack: [3, 3] }
        });
        this.shape = new createjs.Sprite(sh, "idle");
        this.attackDistance = 100;
        this.speed = 10;
    }
    else {
      // demon --> poursuit le joueur jusqu'à une certaine limite
      this.type = 1;
      let sh = new createjs.SpriteSheet({
        images: [loader.get("pitit_demon")],
        frames: {count: 7, width: 32, height: 32},
        animations: { danse: [0, 6, undefined, 0.42 ] }
      });
      this.shape = new createjs.Sprite(sh, "danse");
      this.attackDistance = 200;
    }
    this.shape.scaleX = 1.6;
    this.shape.scaleY = 1.6;
    this.width = 32 * this.shape.scaleX;
    this.height = 32 * this.shape.scaleY;
    stage.addChild(this.shape);

    // collision shape
    this.collisionBox = new createjs.Shape();
    this.collisionBox.graphics.beginStroke("red");
    this.collisionBox.graphics.drawRect(this.width*0.2, this.height*0.1, this.width*0.6, this.height*0.8);
    this.collisionBox.graphics.endStroke();
    stage.addChild(this.collisionBox);
  }

  get bounds() {
    return { x: this.x + this.width * 0.2, y: this.y + this.height*0.1, w: this.width*0.6, h: this.height*0.8 };
  }

  update() {
    this.collisionBox.visible = this.dev;
    this.x += this.vec.x * this.speed;
    this.y += this.vec.y * this.speed;
  }

  isClosedTo(player) {
    return ((this.x-player.x)*(this.x-player.x)+(this.y-player.y)*(this.y-player.y)) < this.attackDistance*this.attackDistance;
  }

}

},{}],3:[function(require,module,exports){
const Level = require('./level.js')
const Player = require('./player.js')

module.exports = class Game {
  constructor(loader, audio, debug) {
    this.loader = loader;
    this.audio = audio;
    this.debug = !!debug;

    this.canvas = null;
    this.started = false;
    this.paused = false;

    this.dev = false;
  }

  setCanvas(canvas) {
    this.canvas = canvas;

    this.canvas.width = this.viewWidth;
    this.canvas.height = this.viewHeight;

    this.stage = new createjs.Stage(this.canvas);
    this.stage.snapToPixel = true;

    this.player = new Player(this.loader, this.stage);
    this.level = new Level(this.loader, this.stage, this.player, this.audio);

    // this.setDevMode(true);
    this.addDebugData();
      
  }

  setDevMode(value) {
    this.level.home.dev = this.player.dev = this.dev = !!value;
    this.level.enemies.forEach((enemy) => enemy.dev = !!value);
    this.level.houses.forEach((house) => house.dev = !!value);
  }

  start() {
    this.paused = false;
    this.started = true;

    this.audio.playMainTheme();
  }

  pause() {
    this.paused = true;
    if (this.player.running)
      this.audio.stopMove();
  }

  resume() {
    this.paused = false;
    if (this.player.running)
      this.audio.playMove();
  }

  stop() {
    this.started = false;

  }

  setTicker() {
    createjs.Ticker.addEventListener("tick", (event) => this.tick(event));
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = 60;
  }

  handleKeydown(key) {
  }

  handleKeyup(key) {
    switch (key) {
      case ' ':
        if (this.level.player.running == false) {
          this.level.resumeGame();
        }
        break;
    }
  }

  handleMousedown(button, x, y, buttons) {
    this.player.increaseSpeed();
  }

  handleMouseup(button, x, y, buttons) {
    this.player.decreaseSpeed();
  }

  handleMousemove(x, y, buttons) {
    this.player.pointTo(x, y);
  }

  addDebugData() {
    if (!this.debug) {
      return;
    }

    const fontSize = 18;

    this.fpsLabel = new createjs.Text("-- fps", "bold " + fontSize + "px Arial", "#FF0");

    this.fpsLabel.x = 10;
    this.fpsLabel.y = 40;

    this.playerPosLabel = new createjs.Text("x, y", "normal " + fontSize + "px Arial", "#F0F");
    this.playerPosLabel.x = 10;
    this.playerPosLabel.y = 60;

    this.stage.addChild(this.fpsLabel);
    this.stage.addChild(this.playerPosLabel);
  }

  renderDebugData() {
    if (!this.debug) {
      return;
    }

    this.fpsLabel.text = Math.round(createjs.Ticker.getMeasuredFPS()) + " fps";
    this.playerPosLabel.text = Math.round(this.player.x) + ", " + Math.round(this.player.y);
  }

  tick(tickEvent) {
    if (!this.started || this.paused) {
      return;
    }

    this.level.update();
    this.stage.update();
    this.renderDebugData();
  }

  get viewWidth() {
    return 960;
  }

  get viewHeight() {
    return 540;
  }
}

},{"./level.js":6,"./player.js":8}],4:[function(require,module,exports){
module.exports = class Home {

    constructor(loader, stage, x, y) {
      this.loader = loader;
      this.stage = stage;

      this.x = x;
      this.y = y;
      this.width = 256;
      this.height = 256;

      this.dev = false;

      // dessin de la maison
      // this.bitmap = new createjs.Bitmap(this.loader.get("tileset-graveyard"));
      this.bitmap = new createjs.Bitmap(this.loader.get("tileset-graveyard"));
      this.bitmap.sourceRect = new createjs.Rectangle(256,0,128,128);
      this.stage.addChild(this.bitmap);

      this.container = new createjs.Container();
      this.container.y = 10;
      this.container.width = stage.canvas.width / 3;
      this.container.regX = this.container.width;
      this.container.x = stage.canvas.width - 10;

      this.collectedItems = [];
      this.itemsToCollect = [1, 2, 3, 4, 5, 6];

      var sp = new createjs.SpriteSheet({
        images: [loader.get("objects")],
        frames: { count: 7, width: 32, height: 32 },
        animations: {
          all: [0, 6]
        }
      });
      this.sprites = [
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp)
      ];
      stage.addChild(this.container);

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width*0.05, this.height*0.12, this.width*0.25, this.height*0.3);
      this.collisionBox.graphics.endStroke();
      stage.addChild(this.collisionBox);
    }

    reset() {
      this.collectedItems = [];
      this.itemsToCollect = [1, 2, 3, 4, 5, 6];
      this.update();
    }

    get bounds() {
      return { x: this.x+ this.width*0.05, y: this.y + this.height*0.12, w: this.width*0.25, h: this.height*0.3 };
    }

    update() {
      this.container.removeAllChildren();
      this.itemsToCollect.forEach((item, i, a) => {
        this.container.addChild(this.sprites[item]);
        this.sprites[item].y = 0;
        this.sprites[item].gotoAndStop(item);
        this.sprites[item].x = this.container.width - (a.length - i) * 32;
      });
      this.collisionBox.visible = this.dev
    }

    putItem(item) {
      var index = this.itemsToCollect.indexOf(item);
      var exists = (index >= 0);
      if (exists) {
        this.collectedItems.push(item);
        this.itemsToCollect.splice(index, 1);
        this.update();
      }
      return exists;
    }

}

},{}],5:[function(require,module,exports){
module.exports = class House {

    constructor(loader, stage, x, y) {
      this.stage = stage;

      this.x = x
      this.y = y;

      this.light = Math.random() < 0.5;
      this.lastChange = Date.now();

      this.item = 0;

      this.dev = false;

      // dessin de la maison
      var sp = new createjs.SpriteSheet({
        images: [loader.get('maison')],
        frames: {count: 4, width: 64, height: 64},
        animations: {
          eteint: [0, 0],
          allume: [1, 3, undefined, 0.1]
        }
      });
      this.shape = new createjs.Sprite(sp, this.light ? "allume" : "eteint");

      // this.shapeFillCommand = this.shape.graphics.beginFill(this.light ? "yellow" : "gray").command;
      // this.shape.graphics.drawRect(-this.width / 2, -this.height/2, this.width, this.height);
      // this.shape.graphics.endFill();
      // // positionnement initial de la maison
      this.stage.addChild(this.shape);

      this.shape.scaleX = this.shape.scaleY = 2;
      this.width = this.height = 64 * this.shape.scaleY;

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width * 0.1, this.height * 0.4, this.width*0.8, this.height * 0.6);
      this.collisionBox.graphics.endStroke();
      stage.addChild(this.collisionBox);

    }

    get bounds() {
      return {x: this.x + this.width * 0.1, y: this.y + this.height * 0.4, w: this.width, h: this.height * 0.6 };
    }

    update() {
      this.collisionBox.visible = this.dev;
      if (Date.now() - this.lastChange > 3000 && Math.random() < 0.001) {
        this.light = !this.light;
        this.lastChange = Date.now();
        this.shape.gotoAndPlay(this.light ? "allume" : "eteint");
      }
    }

}

},{}],6:[function(require,module,exports){
const Map = require('./map.js');
const House = require('./house.js');
const Home = require('./home.js');
const Enemy = require('./enemy.js');

module.exports = class Level {

  constructor(loader, stage, player, audio) {
    this.loader = loader;
    this.stage = stage;
    this.player = player;
    this.audio = audio;
    this.over = false;
    this.map = new Map(this.loader, this.stage, this.player);

    // placement du joueur
    this.player.x = this.map.width / 2;
    this.player.y = this.map.height / 2;
    this.stage.update();
    // maisons
    this.houses = [];
    this.map.houses.forEach((pos, i) => {
      this.houses.push(new House(this.loader, this.stage, pos.x, pos.y));
      // this.houses[i].light = Math.random() < 0.5;
      if (i > 0 && i < 7) {
        this.houses[i].item = i;
      }
    });
    // sa maison
    this.home = new Home(this.loader, this.stage, this.map.width * 0.5 - 32, this.map.height * 0.5 - 64);
    // ennemis
    this.enemies = [];
    this.map.mobs.forEach((pos, i) => {
      this.enemies.push(new Enemy(this.loader, this.stage, pos.x, pos.y));
    });
    // ajout du joueur
    this.player.addHud();


    this.startLabel = new createjs.Text("Press [Space] key to start running", "normal 18px Arial", "#FFF");
    this.startLabel.textAlign = 'center';
    this.startLabel.x = 480;
    this.startLabel.y = 500;
    this.startLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.startLabel.filters = [this.startLabelFilter];

    createjs.Tween.get(this.startLabelFilter, { loop: true })
      .to({redMultiplier: 0, alphaMultiplier: 0.8 }, 1000)
      .to({redMultiplier: 1, alphaMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.startLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.startLabel.cache(-this.startLabel.getMeasuredWidth()/2, 0, this.startLabel.getMeasuredWidth(), this.startLabel.getMeasuredHeight());
    this.stage.addChild(this.startLabel);

    this.overLabel = new createjs.Text("Game Over :'[", "normal 24px Arial", "#FFF");
    this.overLabel.textAlign = 'center';
    this.overLabel.x = 480;
    this.overLabel.y = 60;
    this.overLabel.visible = false;
    this.overLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.overLabel.filters = [this.overLabelFilter];

    createjs.Tween.get(this.overLabelFilter, { loop: true })
      .to({greenMultiplier: 0, blueMultiplier: 0 }, 1000)
      .to({greenMultiplier: 1, blueMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.overLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.overLabel.cache(-this.overLabel.getMeasuredWidth()/2, 0, this.overLabel.getMeasuredWidth(), this.overLabel.getMeasuredHeight());
    this.stage.addChild(this.overLabel);


    this.victoryLabel = new createjs.Text("You win! Well done, Yuri is Happy!", "normal 24px Arial", "#FFF");
    this.victoryLabel.textAlign = 'center';
    this.victoryLabel.x = 480;
    this.victoryLabel.y = 60;
    this.victoryLabel.visible = false;
    this.victoryLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.victoryLabel.filters = [this.victoryLabelFilter];

    createjs.Tween.get(this.victoryLabelFilter, { loop: true })
      .to({redMultiplier: 0, blueMultiplier: 0 }, 1000)
      .to({redMultiplier: 1, blueMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.victoryLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.victoryLabel.cache(-this.victoryLabel.getMeasuredWidth()/2, 0, this.victoryLabel.getMeasuredWidth(), this.victoryLabel.getMeasuredHeight());
    this.stage.addChild(this.victoryLabel);


  }

  stopGame() {
    this.player.running = false;
    this.startLabel.visible = true;
    this.audio.stopMove();
  }

  resumeGame() {
    this.player.running = true;
    this.startLabel.visible = false;
    this.audio.playMove();
  }

  update() {
    this.player.update(this.map);
    this.map.update();
    // update house position w.r.t. player
    this.houses.forEach(function(house) {
      house.update();
      var xy = this.setCoordsInDisplayForEntity(house);
      house.shape.x = house.collisionBox.x = xy.x;
      house.shape.y = house.collisionBox.y = xy.y;
    }.bind(this));
    this.enemies.forEach(function(enemy) {
      enemy.update();
      var xy = this.setCoordsInDisplayForEntity(enemy);
      enemy.shape.x = enemy.collisionBox.x = xy.x;
      enemy.shape.y = enemy.collisionBox.y = xy.y;
    }.bind(this));

    this.home.update();
    var homexy = this.setCoordsInDisplayForEntity(this.home);
    this.home.bitmap.x = this.home.collisionBox.x = homexy.x;
    this.home.bitmap.y = this.home.collisionBox.y = homexy.y;


    this.startLabel.updateCache();
    if (this.over) {
      this.overLabel.visible = true;
      this.overLabel.updateCache();
    }

    if (!this.player.running || this.player.energy == 0) {
      if (this.player.energy == 0 && !this.over) {
        this.audio.stopMainTheme();
        this.audio.stopSecondTheme();
        this.audio.stopMove();
        this.audio.playGameOver();
        this.over = true;
      }
      return;
    }
    if (this.home.itemsToCollect.length == 0) {
      this.player.running = false;
      this.audio.stopMainTheme();
      this.audio.stopSecondTheme();
      this.audio.stopMove();
      this.victoryLabel.visible = true;
      this.victoryLabel.updateCache();
    }
    if (this.intersects(this.player, this.home)) {
      // rentre dans la tombe...
      if (this.player.item != 0) {
        if (this.home.putItem(this.player.item)) {
          this.player.increaseEnergy();
        }
        this.player.item = 0;
        this.audio.stopSecondTheme();
        this.audio.playMainTheme();
      }
      this.player.resetEnergy();
      return;
    }

    for (var i = 0; i < this.houses.length; i++) {
      if (this.intersects(this.houses[i], this.player)) {
        if (this.houses[i].light) {
          // gameover
          this.player.energy = 0;
          this.player.update();
        }
        else {
          if (this.houses[i].item > 0 && this.player.item == 0) {
            this.player.item = this.houses[i].item;
            this.houses[i].item = 0;
            this.audio.stopMainTheme();
            this.audio.playInHouse();
            this.audio.playSecondTheme();
          }
        }
      }
    }

    // enemies
    if (this.player.isDashing()) {
      return;
    }
    this.enemies.forEach((enemy, i) => {
      if (this.intersects(enemy, this.player)) {
        // gameover
        this.player.energy = 0;
        enemy.vec.x = enemy.vec.y = 0;
        this.audio.stopMove();
        this.audio.stopMainTheme();
        this.audio.stopSecondTheme();

        if (enemy.type == 0) {
          enemy.shape.gotoAndStop("attack");
          enemy.x = this.player.x - this.player.width / 2;
          enemy.y = this.player.y - this.player.height * 0.7;
          this.audio.playCat();
        } else {
          this.audio.playDemon();
        }

        this.audio.playGameOver(true);
        this.over = true;

        return;
      }
      if (enemy.isClosedTo(this.player)) {
        let dist = Math.sqrt((enemy.x-this.player.x)*(enemy.x-this.player.x)+(enemy.y-this.player.y)*(enemy.y-this.player.y));
        enemy.vec.x = (this.player.x - enemy.x) / dist;
        enemy.vec.y = (this.player.y - enemy.y) / dist;
      }
      else {
        enemy.vec.x = enemy.vec.y = 0;
      }
    });

  }

  intersects(rect1, rect2) {
      if ( rect1.bounds.x >= rect2.bounds.x + rect2.bounds.w ||  // rect1 à droite de rect2
           rect1.bounds.x + rect1.bounds.w <= rect2.bounds.x ||  // rect1 à gauche de rect2 
           rect1.bounds.y >= rect2.bounds.y + rect2.bounds.h ||  // rect1 au dessous de rect2
           rect1.bounds.y + rect1.bounds.h <= rect2.bounds.y )  //  rect1 au dessus de rect2
          return false;
      return true;
  }

  // assume entity has .x and .y properties for positions in world
  setCoordsInDisplayForEntity(entity) {
    var deltaX = entity.x - this.player.x;
    var deltaY = entity.y - this.player.y;
    // dépasse au dessus :    // rajouter la taille du bloc
    if (this.player.y < this.player.shape.y) {
      var debord = this.player.shape.y - this.player.y;
      if (entity.y + entity.height >= this.map.height - debord) {
      deltaY -= this.map.height;
      }
    }
    // dépasse en dessous :
    else if (this.player.y > this.map.height - this.player.shape.y) {
      var debord = this.player.shape.y - (this.map.height - this.player.y);
      if (entity.y - entity.height <= debord) {
        deltaY += this.map.height;
      }
    }

    // dépasse à gauche
    if (this.player.x < this.player.shape.x) {
      var debord = this.player.shape.x - this.player.x;
      if (entity.x + entity.width >= this.map.width - debord) {
        deltaX -= this.map.width;
      }
    }
    // dépasse à droite
    else if (this.player.x > this.map.width - this.player.shape.x) {
      var debord = this.player.x - (this.map.width - this.player.shape.x);
      if (entity.x - entity.width <= debord) {
        deltaX += this.map.width;
      }
    }

    return {x: (deltaX + this.stage.canvas.width / 2), y: (deltaY + this.stage.canvas.height / 2)};
  }
}

},{"./enemy.js":2,"./home.js":4,"./house.js":5,"./map.js":7}],7:[function(require,module,exports){
module.exports = class Map {

  constructor(loader, stage, player) {
    this.stage = stage;
    this.loader = loader;

    this.map = new createjs.Container();
    this.map.x = 0;
    this.map.y = 0;

    this.stage.addChild(this.map);

    this.tileW = 32;
    this.tileH = 32;

    this.player = player;

    this.nbMobs = 42;
    this.nbHouse = 10;
    this.nbItemGraveyard = 20;
    this.nbItemMap = 400;
    this.nbFence = 100;
    this.nbItemHouse = 7;

    var nbCaseX = Math.floor(Math.random() * (90 - 30)) + 180;
    var nbCaseY = Math.floor(Math.random() * (48 - 12)) + 100;
    this.worldGeneration = this.generateMap(nbCaseX, nbCaseY);
    this.matrix = [];
    this.houses = [];
    this.mobs = [];
    //Génération des sols
    //Génération du fond de map
    for (var i=0; i < nbCaseY; i++) {   
      this.matrix.push([]);
      for (var j=0; j < nbCaseX; j++) {
        this.matrix[i].push((this.worldGeneration[i*nbCaseX+j] ? "1" : "2"));
      }
    }

    //Enleve cases seules
    for (var i=1; i < nbCaseY-1; i++) {
      for (var j=1; j < nbCaseX-1; j++) {
        this.matrix[i].push((this.worldGeneration[i*nbCaseX+j] ? "1" : "2"));
      }
    }

    //Génération de la tombe
    let midX = Math.floor(nbCaseX/2);
    let midY = Math.floor(nbCaseY/2);
    for (var i=midY-4; i<midY+4; i++) {
      for (var j=midX-4; j<midX+4; j++) {
        this.matrix[i][j]="1";
      }
    }

    //Exclusion autour du mausolé
    for (var i=midY-3; i<midY+3; i++) {
      for (var j=midX-3; j<midX+3; j++) {
        this.matrix[i][j] += "0";
      }
    }

    //Ajout d'objets de décoration à proximité du cimetiere
    for (var i=0; i<this.nbItemGraveyard ; i++) {
      let randX = midX - 5 + Math.floor(Math.random() * 10);
      let randY = midY - 5 + Math.floor(Math.random() * 10);
      if(this.matrix[randY][randX].length < 2){
        this.matrix[randY][randX] += Math.floor(Math.random() * 4) + 1;
      }
    } 

    //Exclusion autour du cimetiere
    for (var i=midY-5; i<midY+5; i++) {
      for (var j=midX-5; j<midX+5; j++) {
        if(this.matrix[i][j].length < 2){
          this.matrix[i][j] += "0";
        }
      }
    }

    //Ajout de pierres
    for (var i=midY-4; i<midY+3; i++) {
      for (var j=midX-3; j<midX+3; j++) {
        switch (Math.floor(Math.random() * 8)) {
          case 0:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'V';
            break;
          case 1:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'W';
            break;
          case 2:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'X';
            break;
          case 3:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'Y';
            break;
          case 4:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'Z';
            break;
        }
      }
    }

    //Ajout d'objets dans la map
    for (var i=0; i<this.nbItemMap ; i++) {
      let randX = Math.floor(Math.random() * this.matrix[0].length);
      let randY = Math.floor(Math.random() * this.matrix.length);
      if(this.matrix[randY][randX].length < 2){
        let alea = Math.floor(Math.random() * 5) + 5;
        if(alea == 9){
          if(this.matrix[randY][randX].charAt(0) == 2 ){
            this.matrix[randY][randX] += alea;
          }
        }else{
          this.matrix[randY][randX] += alea;
        }
      }
    } 

    //Aucun objet de décorations
    for (var i=0; i < nbCaseY; i++) {
      for (var j=0; j < nbCaseX; j++) {
        if(this.matrix[i][j].length < 2){
          this.matrix[i][j] += "0";
        }
      }
    }

    //Ajout des barrières
    this.matrix[midY+5][midX+2] = this.matrix[midY+5][midX+2].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-2] = this.matrix[midY+5][midX-2].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-1] = this.matrix[midY+5][midX-1].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-3] = this.matrix[midY+5][midX-3].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-2] = this.matrix[midY-5][midX-2].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-1] = this.matrix[midY-5][midX-1].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-3] = this.matrix[midY-5][midX-3].substr(0, 1) + 'T';

    //Calcul des positions des maisons
    for(var i=0; i<this.nbHouse; i++){
      let Coordx = 0;
      let Coordy = 0;
      do{
        Coordx = Math.floor(Math.random() * this.matrix[0].length);
        Coordy = Math.floor(Math.random() * this.matrix.length);
      }while(!this.canBuild(Coordx,Coordy));
      //Push tableau des maisons
      this.matrix[Coordy][Coordx] = this.matrix[Coordy][Coordx].substr(0, 1) + 'H';
      Coordx *= this.tileW 
      Coordy *= this.tileH
      this.houses.push({x:Coordx, y:Coordy});
    }

    //Ajout dd'objets autour de maisons
    this.houses.forEach((house) => {
      for (var i=0; i<this.nbItemHouse ; i++) {
        let randX = (house.x/this.tileW ) - 4 + Math.floor(Math.random() * 8);
        let randY = (house.y/this.tileH )- 4 + Math.floor(Math.random() * 8);
        if(this.matrix[randY][randX].charAt(1) != 'H'){
          switch (Math.floor(Math.random() * 4)) {
            case 0:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'A';
              break;
            case 1:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'B';
              break;
            case 2:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'C';
              break;
            case 3:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'D';
              break;
          }
        }
      } 

      this.tilesCorrespondance = {
        1: new createjs.Rectangle(0, 96, 64, 64),
        2: new createjs.Rectangle(64, 96, 64, 64),
        3: new createjs.Rectangle(96, 32, 64, 64),
        4: new createjs.Rectangle(32, 64, 64, 64),
        5: new createjs.Rectangle(0, 0, 64, 64),
        6: new createjs.Rectangle(32, 0, 64, 64),
        7: new createjs.Rectangle(128, 32, 64, 64),
        8: new createjs.Rectangle(180, 3, 32, 32),
        9: new createjs.Rectangle(159, 193, 32, 32),
        A: new createjs.Rectangle(0, 192, 64, 64),
        B: new createjs.Rectangle(32, 192, 64, 64),
        C: new createjs.Rectangle(32, 128, 64, 64),
        D: new createjs.Rectangle(64, 128, 64, 64),
        T: new createjs.Rectangle(32, 320, 256, 256),
        V: new createjs.Rectangle(0, 160, 64, 64),
        W: new createjs.Rectangle(0, 192, 64, 64),
        X: new createjs.Rectangle(32, 192, 64, 64),
        Y: new createjs.Rectangle(64, 160, 64, 64),
        Z: new createjs.Rectangle(32, 160, 64, 64)
      }

    })


    //Calcul des positions des mobs dans le champs
    for(var i=0; i<this.nbMobs; i++){
      let Coordx = 0;
      let Coordy = 0;
      do{
        Coordx = Math.floor(Math.random() * this.matrix[0].length);
        Coordy = Math.floor(Math.random() * this.matrix.length);
      }while(!this.canSpawn(Coordx,Coordy));
      //Push tableau des mobs
      this.matrix[Coordy][Coordx] = this.matrix[Coordy][Coordx].substr(0, 1) + 'M';
      Coordx *= this.tileW 
      Coordy *= this.tileH
      this.mobs.push({x:Coordx, y:Coordy});
    } 
  }

  get width() {
    return this.matrix[0].length*this.tileW;
  }

  get height() {
    return this.matrix.length*this.tileH;
  } 

  update() {
    this.map.removeAllChildren();

    let longueur = this.tileW * this.matrix[0].length;
    if (this.player.x > longueur) {
      this.player.x -= longueur;
    }
    else if (this.player.x < 0) {
      this.player.x = longueur + this.player.x;
    }

    let hauteur = this.tileH * this.matrix.length;
    if (this.player.y > hauteur) {
      this.player.y -= hauteur;
    }
    else if (this.player.y < 0) {
      this.player.y = hauteur + this.player.y;
    }


    var player1_tile_x = Math.floor(this.player.x / this.tileW);
    var player1_tile_y = Math.floor(this.player.y / this.tileH);

    let start_x = player1_tile_x - Math.floor(this.stage.canvas.width / 2 / this.tileW) - 1;
    let end_x = player1_tile_x + Math.floor(this.stage.canvas.width / 2 / this.tileW) + 2;
    let start_y = player1_tile_y - Math.floor(this.stage.canvas.height / 2 / this.tileH) - 1;
    let end_y = player1_tile_y + Math.floor(this.stage.canvas.height / 2 / this.tileH) + 2;

    for (var i = start_x; i < end_x; ++i) {
      for (var j = start_y; j < end_y; ++j) {
        var myI = (i < 0) ? this.matrix[0].length + i : i;
        var myJ = (j < 0) ? this.matrix.length + j : j;
        myI = (myI >= this.matrix[0].length) ? myI - this.matrix[0].length: myI;
        myJ = (myJ >= this.matrix.length) ? myJ - this.matrix.length: myJ;

        var x = (this.stage.canvas.width / 2) - ((this.player.x - ((i) * this.tileW)));
        var y = (this.stage.canvas.height / 2) - ((this.player.y - ((j) * this.tileH)));
        var bitmap = null;
        try {
          //Affichage sol
          switch (this.matrix[myJ][myI].charAt(0)) {
            case "1":
              var color = "#65845c";
              break;
            case "2":
              var color = "#3a5941";
              break;
          }

          if (this.tilesCorrespondance[this.matrix[myJ][myI].charAt(1)]) {
            bitmap = new createjs.Bitmap(this.loader.get('tileset-graveyard'));
            bitmap.sourceRect = this.tilesCorrespondance[this.matrix[myJ][myI].charAt(1)];
            bitmap.x = x;
            bitmap.y = y;
          }
        } catch (error) {
          debugger;
        }

        var rect = new createjs.Shape();
        rect.graphics.beginFill(color);
        rect.graphics.drawRect(0, 0, this.tileW+1, this.tileH+1);
        rect.graphics.endFill();
        rect.x = x;
        rect.y = y;
        this.map.addChild(rect);
          
        if (bitmap) {
          this.map.addChild(bitmap);
        }
      }
    }
  }

  generateMap(nbCaseX, nbCaseY){
    let tileArray = new Array();
    let probabilityModifier = 0;
    let landMassAmount=1; // scale of 1 to 5
    let landMassSize=3; // scale of 1 to 5
    let rndm;
    let probability = 15 + landMassAmount;
    let conformity;

    for (let i = 0; i < nbCaseX*nbCaseY; i++) {
        if (i>(nbCaseX*2)+2){
          // Conform the tile upwards and to the left to its surroundings
          conformity =
            (tileArray[i-nbCaseX-1]==(tileArray[i-(nbCaseX*2)-1]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-nbCaseX]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-1]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-nbCaseX-2]));
          if (conformity<2){
            tileArray[i-nbCaseX-1]=!tileArray[i-nbCaseX-1];
          }
        }
        // get the probability of what type of tile this would be based on its surroundings
        probabilityModifier = (tileArray[i-1]+tileArray[i-nbCaseX]+tileArray[i-nbCaseX+1])*(19+(landMassSize*1.4));
        rndm=(Math.random()*101);
        tileArray[i]=(rndm<(probability+probabilityModifier));
    }
    return tileArray; 
  }

  canBuild(x,y){
    let partX = this.matrix[0].length / 8;
    let partY = this.matrix.length / 8;
    if (x < partX || x > 7 * partX || y < partY || y > 7 * partY) {
      return false;
    }
    if (x > 2 * partX && x < 6 * partX && y > 2 * partY && y < 6 * partY) {
      return false;
    }
    let
      lowx = Math.max(x-4, 0),
      highx = Math.min(x+4, this.matrix[0].length),
      lowy = Math.max(y-4, 0),
      highy = Math.min(y+4, this.matrix.length)
    ;
    for (var i=lowy; i < highy; i++) {
      for (var j=lowx; j < highx; j++) {
        if(this.matrix[i][j].charAt(1) != "0"){
          return false;
        }
      }
    }
    return true;
  }

  canSpawn(x,y){
    let partX = this.matrix[0].length / 8;
    let partY = this.matrix.length / 8;
    if (x < partX || x > 7 * partX || y < partY || y > 7 * partY) {
      return false;
    }
    if (x > 3 * partX && x < 5 * partX && y > 3 * partY && y < 5 * partY) {
      return false;
    }
    let
      lowx = Math.max(x-4, 0),
      highx = Math.min(x+4, this.matrix[0].length),
      lowy = Math.max(y-4, 0),
      highy = Math.min(y+4, this.matrix.length)
    ;
    for (var i=lowy; i < highy; i++) {
      for (var j=lowx; j < highx; j++) {
        if(this.matrix[i][j].charAt(1) != "0"){
          return false;
        }
      }
    }
    return true;
  }
  
}





},{}],8:[function(require,module,exports){
module.exports = class Player {

  constructor(loader, stage) {
      this.loader = loader;

      this.x = stage.canvas.width/2;
      this.y = stage.canvas.height/2;
      this.vec = {x: 0, y: 0};
      this.nextVec = {x: 0, y: -1};
      this.stage = stage;

      this.initSpeed = 4;
      this.speed = this.initSpeed;

      this.initEnergy = 100;
      this.maxEnergy = this.initEnergy;
      this.energy = this.initEnergy;

      this.initDeltaNRJ = 0.01;
      this.deltaNRJ = this.initDeltaNRJ;

      this.running = false;
      this.dev = false;

      // item portée par le personnage (0: aucun, 1: item1, 2: item2, etc.)
      this.item = 0;

      // spritesheets du personnage
      var data = {
        images: [
          this.loader.get("ghost_go_down"),
          this.loader.get("ghost_go_up"),
          this.loader.get("ghost_go_left"),
          this.loader.get("ghost_go_right"),
          this.loader.get("ghost_dash_down"),
          this.loader.get("ghost_dash_up"),
          this.loader.get("ghost_dash_left"),
          this.loader.get("ghost_dash_right"),
          this.loader.get("ghost_dash_right_down"),
          this.loader.get("ghost_dash_right_up"),
          this.loader.get("ghost_dash_left_down"),
          this.loader.get("ghost_dash_left_up"),
          this.loader.get("ghost_go_in_house")
        ],
        frames: { count: 73, width: 32, height: 32 },
        animations: {
          go_down: [0, 5, undefined, 0.42 ],
          go_up: [6, 11, undefined, 0.42 ],
          go_left: [12, 17, undefined, 0.42 ],
          go_right: [18, 23, undefined, 0.42 ],
          dash_down: [24, 27, "dash_down_wait", 0.42 ],
          dash_down_wait: [27, 27, undefined, 0.42],
          dash_down_end: [27, 29, "go_down", 0.15],
          dash_up: [30, 33, "dash_up_wait", 0.42 ],
          dash_up_wait: [33, 33, undefined, 0.42 ],
          dash_up_end: [33, 35, "go_up", 0.15 ],
          dash_left: [36, 39, "dash_left_wait", 0.42 ],
          dash_left_wait: [39, 39, undefined, 0.42 ],
          dash_left_end: [39, 41, "go_left", 0.15 ],
          dash_right: [42, 45, "dash_right_wait", 0.42 ],
          dash_right_wait: [45, 45, undefined, 0.42 ],
          dash_right_end: [45, 47, "go_right", 0.15 ],
          
          dash_right_down: [48, 51, "dash_right_down_wait", 0.42],
          dash_right_down_wait: [51, 51, undefined, 0.42],
          dash_right_down_end: [51, 53, "go_right", 0.42],

          dash_right_up: [54, 57, "dash_right_up_wait", 0.42 ],
          dash_right_up_wait: [57, 57, undefined, 0.42 ],
          dash_right_up_end: [57, 59, "go_right", 0.42 ],

          dash_left_down: [60, 63, "dash_left_down_wait", 0.42 ],
          dash_left_down_wait: [63, 63, undefined, 0.42 ],
          dash_left_down_end: [63, 65, "go_left", 0.42 ],

          dash_left_up: [66, 69, "dash_left_up_wait", 0.42 ],
          dash_left_up_wait: [69, 69, undefined, 0.42 ],
          dash_left_up_end: [69, 71, "go_left", 0.42 ],

          go_in_house: [72]
        }
      };
      var ghostSpriteSheet = new createjs.SpriteSheet(data);
      this.currentAnimation = "go_down";
      this.shape = new createjs.Sprite(ghostSpriteSheet, this.currentAnimation);

      //this.shape = new createjs.Bitmap("./assets/game/ghost.png");
      this.shape.regX = 16;
      this.shape.regY = 16;
      this.shape.scaleX = 1.4;
      this.shape.scaleY = 1.4;
      //this.stage.addChild(this.shape);

      this.width = 32 * this.shape.scaleX;
      this.height = 32 * this.shape.scaleY;

      // éventuel item du personnage
      var sp = new createjs.SpriteSheet({
        images: [loader.get("objects")],
        frames: { count: 7, width: 32, height: 32 },
        animations: {
          all: [0, 6]
        }
      });
      this.carriedItem = new createjs.Sprite(sp);
      this.carriedItem.gotoAndStop(this.item);
      this.carriedItem.x = this.stage.canvas.width / 2 + 10;
      this.carriedItem.y = this.stage.canvas.height / 2 - this.height;
      this.carriedItem.scaleX = 0.8;
      this.carriedItem.scaleY = 0.8;

      // barre de vie du personnage
      this.barre = new createjs.Shape();
      this.barre.width = stage.canvas.width / 3;
      this.barre.height = 20;
      this.barre.x = 10;
      this.barre.y = 10;
      this.barre.graphics.setStrokeStyle(1);
      this.barre.graphics.beginStroke("#000000");
      this.barre.graphics.drawRect(-1, -1, this.barre.width + 2, this.barre.height + 2);
      this.barre.graphics.endStroke();
      // jauge
      this.jauge = new createjs.Shape();
      this.jaugeFillCommand = this.jauge.graphics.beginFill("hsl(" + (130 * this.energy/100) + ", 100%, 50%)").command;
      this.jauge.graphics.drawRect(0, 0, this.barre.width, this.barre.height);
      this.jauge.graphics.endFill();
      this.jauge.x = 10;
      this.jauge.y = 10;
      // fleche
      this.fleche = new createjs.Bitmap(this.loader.get("fleche"));
      this.fleche.x = 0;
      this.fleche.y = 0;
      this.fleche.regX = 16;
      this.fleche.regY = 16;
      this.fleche.visible = false;

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width *0.25, this.height*0.25, this.width*0.5, this.height*0.5);
      this.collisionBox.graphics.endStroke();

      // positionnement du personnage --> toujours au milieu de l'écran
      this.shape.x = this.stage.canvas.width / 2;
      this.collisionBox.x = this.stage.canvas.width / 2 - this.width / 2 | 0;
      this.shape.y = this.stage.canvas.height / 2;
      this.collisionBox.y = this.stage.canvas.height / 2 - this.height / 2 | 0;

  }

  addHud() {
    this.stage.addChild(this.shape);
    this.stage.addChild(this.collisionBox);
    this.stage.addChild(this.carriedItem);
    this.stage.addChild(this.fleche);
    this.stage.addChild(this.barre);
    this.stage.addChild(this.jauge);
  }

  get bounds() {
    return { x: this.x - this.width * 0.25, y: this.y - this.height * 0.25, w: this.width * 0.5, h: this.height * 0.5 };
  }

  // recalcul du vecteur de déplacement
  pointTo(x, y) {
      var rect = this.stage.canvas.getBoundingClientRect();
      x = x - rect.left;
      y = y - rect.top;
      let x0 = this.stage.canvas.clientWidth / 2;
      let y0 = this.stage.canvas.clientHeight / 2;
      let distX = x-x0;
      let distY = y-y0;
      let dist = Math.sqrt(distX*distX+distY*distY);
      this.nextVec = { x: distX / dist, y: distY / dist };
  }

  increaseSpeed() {
      if (!this.running) return;
    this.speed = this.initSpeed * 3;
    this.deltaNRJ = this.initDeltaNRJ * 3;
    var cost = this.computeNextCostume();
    this.lastDash = "dash" + cost.substring(2);
    switch (this.lastDash) {
      case "dash_right":
        if (this.vec.y > 0.3) {
          this.lastDash = "dash_right_down";
        }
        else if (this.vec.y < -0.3) {
          this.lastDash = "dash_right_up";
        }
        break;
      case "dash_left": 
        if (this.vec.y > 0.3) {
          this.lastDash = "dash_left_down";
        }
        else if (this.vec.y < -0.3) {
          this.lastDash = "dash_left_up";
        }
        break;
      case "dash_up": 
        if (this.vec.x > 0.3) {
          this.lastDash = "dash_right_up";
        }
        else if (this.vec.x < -0.3) {
          this.lastDash = "dash_left_up";
        }
        break;
      case "dash_down": 
        if (this.vec.x > 0.3) {
          this.lastDash = "dash_right_down";
        }
        else if (this.vec.x < -0.3) {
          this.lastDash = "dash_left_down";
        }
        break;
    }
    this.shape.gotoAndPlay(this.lastDash);
  }



  decreaseSpeed() {
    var cost = this.computeNextCostume();
    this.currentAnimation = cost;
    this.shape.gotoAndPlay(this.lastDash + "_end");  
    this.speed = this.initSpeed;
    this.deltaNRJ = this.initDeltaNRJ;
  }

  isDashing() {
    return this.speed != this.initSpeed;
  }

  reset(map) {
    this.speed = this.initSpeed;
    this.energy = this.initEnergy;
    if (map) {
      this.x = map.width / 2;
      this.y = map.height / 2;
    }
  }

  increaseEnergy() {
    this.maxEnergy *= 1.2;
  }
  resetEnergy() {
    this.energy = this.maxEnergy;
  }

  update(map) {
    this.collisionBox.visible = this.dev;
    if (!this.running) {
      return;
    }
    if (this.energy > 0) {
      // déplacement du personnage
      if (! this.isDashing()) {
        this.vec.x = this.nextVec.x;
        this.vec.y = this.nextVec.y;
      }
      this.x += this.vec.x * this.speed;
      this.y += this.vec.y * this.speed;
      // costume du personnage
      if (this.speed == this.initSpeed) {
        var nextCostume = this.computeNextCostume();
        if (nextCostume != this.currentAnimation) {
          this.currentAnimation = nextCostume;
          this.shape.gotoAndPlay(this.currentAnimation);
        }
      }
      // consommation d'énergie
      var delta = (this.dev ? this.initDeltaNRJ : this.deltaNRJ);

      this.energy -= (this.speed * delta * (this.dev ? 0.5 : 1));
      if (this.energy < 0) {
        this.energy = 0;
      }
      // fleche
      var x0 = map.width / 2;
      var y0 = map.height / 2;
      var dist = Math.sqrt((this.x-x0)*(this.x-x0)+(this.y-y0)*(this.y-y0));
      var vec = { x: (x0 - this.x)/dist, y: (y0 - this.y)/dist };
      // console.log(vec);
      var inX = (Math.abs(x0 - this.x) < this.stage.canvas.width / 2);
      var inY = (Math.abs(y0 - this.y) < this.stage.canvas.height / 2);
      this.fleche.x = this.stage.canvas.width / 2 + (this.stage.canvas.width / 2 - 100) * vec.x;
      this.fleche.y = this.stage.canvas.height / 2 + (this.stage.canvas.height / 2 - 100) * vec.y;
      this.fleche.rotation = Math.acos(vec.y) * 180 / Math.PI;
      if (x0 > this.x) {
        this.fleche.rotation = 360 - this.fleche.rotation;
      }
      // cacher fleche
      this.fleche.visible =  !inX || !inY;
    }
    if (this.energy == 0) {
      this.shape.gotoAndStop("go_in_house");
    }
    // redessin de la barre hsl(130,100%,50%)
    if (this.speed == this.initSpeed) {
      this.jaugeFillCommand.style = "hsl(" + (130 * this.energy/this.maxEnergy) + ", 100%, 50%)";
    }
    else {
      this.jaugeFillCommand.style = "red";
    }
    this.jauge.scaleX = (this.energy / this.maxEnergy);
    this.carriedItem.gotoAndStop(this.item);

  }

  computeNextCostume() {
    if (this.vec.x > 0.85) {
      return "go_right";
    }
    if (this.vec.x < -0.85) {
      return "go_left";
    }
    if (this.vec.y > 0) {
      return "go_down"
    }
    return "go_up";
  }


}

},{}],9:[function(require,module,exports){
module.exports = class Loader {
  constructor() {
    this.queue = new createjs.LoadQueue();
    this.queue.installPlugin(createjs.Sound);
  }

  setProgressCallback(callback) {
    if (this.progressListener) {
      this.queue.off('progress', this.progressListener);
    }

    if (callback) {
      this.progressListener = this.queue.on(
        'progress',
        event => callback(event.loaded, event.progress, event.total)
      );
    }

    return this;
  }

  get(id) {
    return this.queue.getResult(id);
  }

  loadManifest(manifest) {
    return new Promise((resolve, reject) => {
      this.queue.on('complete', resolve);
      this.queue.loadManifest({ src: manifest, type: 'manifest' });
    });
  }
}

},{}],10:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class AboutMenu extends BaseMenu {
  get documentSelector() {
    return '#aboutScreen';
  }

  get closeButton() {
    return this.get('#closeAboutButton');
  }

  setupMenu() {
    this.addMenuItem('close', this.closeButton);
  }
}

},{"./base-menu.js":11}],11:[function(require,module,exports){
module.exports = class BaseMenu {
  constructor(document) {
    this.document = document;
    this.element = document.querySelector(this.documentSelector);
    this.menuItems = {};

    this.setupMenu();
  }

  addMenuItem(name, element) {
    this.menuItems[name] = element;
  }

  get(selector) {
    this[`_${selector}`] = this[`_${selector}`] || this.document.querySelector(selector);

    return this[`_${selector}`];
  }

  show() {
    this.element.style.display = 'block';
  }

  hide() {
    this.element.style.display = 'none';
  }

  on(menuItem, callback) {
    this.menuItems[menuItem].addEventListener('click', callback);
  }
}

},{}],12:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class GameMenu extends BaseMenu {
  get documentSelector() {
    return '#gameScreen';
  }

  get pauseButton() {
    return this.get('#pauseButton')
  }

  get muteButton() {
    return this.get('#muteGameButton');
  }

  setupMenu() {
    this.addMenuItem('pause', this.pauseButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}

},{"./base-menu.js":11}],13:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class LevelMenu extends BaseMenu {
  get documentSelector() {
    return '#levelSelectionScreen';
  }

  get howtoButton() {
    return this.get('#howtoButton')
  }

  get startButton() {
    return this.get('#startLevelButton')
  }

  get cancelButton() {
    return this.get('#cancelLevelSelectionButton');
  }

  setupMenu() {
    this.addMenuItem('start', this.startButton);
    this.addMenuItem('cancel', this.cancelButton);
    this.addMenuItem('howto', this.howtoButton);
  }
}

},{"./base-menu.js":11}],14:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class MainMenu extends BaseMenu {
  get documentSelector() {
    return '#mainScreen';
  }

  get startButton() {
    return this.get('#startButton');
  }

  get aboutButton() {
    return this.get('#aboutButton');
  }

  get scoresButton() {
    return this.get('#scoresButton');
  }

  get muteButton() {
    return this.get('#muteMainButton');
  }

  setupMenu() {
    this.addMenuItem('start', this.startButton);
    this.addMenuItem('about', this.aboutButton);
    this.addMenuItem('scores', this.scoresButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}

},{"./base-menu.js":11}],15:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class ScoreMenu extends BaseMenu {
  get documentSelector() {
    return '#pauseScreen';
  }

  get resumeButton() {
    return this.get('#resumeButton');
  }

  get quitButton() {
    return this.get('#quitButton');
  }

  get muteButton() {
    return this.get('#mutePauseButton');
  }

  setupMenu() {
    this.addMenuItem('resume', this.resumeButton);
    this.addMenuItem('quit', this.quitButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}

},{"./base-menu.js":11}],16:[function(require,module,exports){
const BaseMenu = require('./base-menu.js')

module.exports = class ScoreMenu extends BaseMenu {
  get documentSelector() {
    return '#scoresScreen';
  }

  get closeButton() {
    return this.get('#closeScoresButton');
  }

  setupMenu() {
    this.addMenuItem('close', this.closeButton);
  }
}

},{"./base-menu.js":11}],17:[function(require,module,exports){
module.exports = class InputsManager {
  constructor(document) {
    this.document = document;
    this.listeners = {
      keydown: [],
      keyup: [],
      touchstart: [],
      touchmove: [],
      touchend: [],
      mousemove: [],
      mousedown: [],
      mouseup: []
    };

    this.keypressed = {};

    this.setHandlers();
  }

  setHandlers() {
    this.setKeyboardHandlers();
    this.setTouchHandlers();
    this.setMouseHandlers();
  }

  setKeyboardHandlers() {
    this.document.addEventListener('keydown', (event) => this.keydownHandler(event.key));
    this.document.addEventListener('keyup', (event) => this.keyupHandler(event.key));
  }

  setTouchHandlers() {
    return;
    this.document.addEventListener('touchstart', (event) => { this.touchstartHandler(event.touches, event.changedTouches); event.preventDefault(); });
    this.document.addEventListener('touchmove', (event) => { this.touchmoveHandler(event.touches, event.changedTouches); event.preventDefault(); });
    this.document.addEventListener('touchend', (event) => { this.touchendHandler(event.touches, event.changedTouches); event.preventDefault(); });
  }

  setMouseHandlers() {
    this.document.addEventListener('mousemove', (event) => this.mousemoveHandler(event.clientX, event.clientY, event.buttons));
    this.document.addEventListener('mousedown', (event) => this.mousedownHandler(event.button, event.clientX, event.clientY, event.buttons));
    this.document.addEventListener('mouseup', (event) => this.mouseupHandler(event.button, event.clientX, event.clientY, event.buttons));
  }

  keydownHandler(key) {
    if (this.keypressed[key]) {
      return;
    }

    this.keypressed[key] = true;
    this.listeners.keydown.forEach(callback => callback(key));
  }

  keyupHandler(key) {
    delete this.keypressed[key]
    this.listeners.keyup.forEach(callback => callback(key));
  }

  touchstartHandler(touches, changedTouches) {
    this.listeners.touchstart.forEach(callback => callback(touhes, changedTouches));
  }

  touchmoveHandler(touches, changedTouches) {
    this.listeners.touchmove.forEach(callback => callback(touhes, changedTouches));
  }

  touchendHandler(touches, changedTouches) {
    this.listeners.touchend.forEach(callback => callback(touhes, changedTouches));
  }

  mousemoveHandler(x, y, buttons) {
    this.listeners.mousemove.forEach(callback => callback(x, y, buttons));
  }

  mousedownHandler(button, x, y, buttons) {
    this.listeners.mousedown.forEach(callback => callback(button, x, y, buttons));
  }

  mouseupHandler(button, x, y, buttons) {
    this.listeners.mouseup.forEach(callback => callback(button, x, y, buttons));
  }

  on(event, callback) {
    this.listeners[event].push(callback);
  }
}

},{}],18:[function(require,module,exports){
const MainMenu = require('./Menus/main-menu.js')
const AboutMenu = require('./Menus/about-menu.js')
const ScoreMenu = require('./Menus/score-menu.js')
const LevelMenu = require('./Menus/level-menu.js')
const PauseMenu = require('./Menus/pause-menu.js')
const GameMenu = require('./Menus/game-menu.js')

module.exports = class MenuManager {
  constructor(document) {
    this.document = document;

    this.mainMenu = new MainMenu(this.document);
    this.aboutMenu = new AboutMenu(this.document);
    this.scoresMenu = new ScoreMenu(this.document);
    this.levelMenu = new LevelMenu(this.document);
    this.pauseMenu = new PauseMenu(this.document);
    this.gameMenu = new GameMenu(this.document);

    this.listeners = {
      start: [],
      pause: [],
      resume: [],
      toggleMute: [],
      quit: []
    };
  }

  setupMenus() {
    this.mainMenu.on('start', () => this.selectLevel());
    this.mainMenu.on('about', () => this.showAbout());
    this.mainMenu.on('scores', () => this.showScores());
    this.mainMenu.on('toggleMute', () => this.toggleMute());

    this.aboutMenu.on('close', () => this.showMainMenu());

    this.scoresMenu.on('close', () => this.showMainMenu());

    this.levelMenu.on('cancel', () => this.showMainMenu());
    this.levelMenu.on('howto', () => this.showHowto());
    this.levelMenu.on('start', () => this.startGame());

    this.pauseMenu.on('resume', () => this.resumeGame());
    this.pauseMenu.on('quit', () => this.quitGame());
    this.pauseMenu.on('toggleMute', () => this.toggleMute());

    this.gameMenu.on('pause', () => this.pauseGame());
    this.gameMenu.on('toggleMute', () => this.toggleMute());
  }

  selectLevel() {
    this.mainMenu.hide();
    this.levelMenu.show();
  }

  showHowto() {
    window.document.querySelector('#history').style.display = 'none';
    window.document.querySelector('#howtoPlay').style.display = 'block';
    this.levelMenu.howtoButton.style.display = 'none';
  }

  startGame() {
    this.mainMenu.hide();
    this.levelMenu.hide();
    this.aboutMenu.hide();
    this.pauseMenu.hide();
    this.scoresMenu.hide();
    this.gameMenu.show();
    this.listeners.start.forEach(callback => callback());
  }

  pauseGame() {
    // this.gameMenu.hide();
    this.pauseMenu.show();
    this.listeners.pause.forEach(callback => callback());
  }

  showMainMenu() {
    this.aboutMenu.hide();
    this.scoresMenu.hide();
    this.levelMenu.hide();
    this.mainMenu.show();
  }

  showAbout() {
    this.mainMenu.hide();
    this.aboutMenu.show();
  }

  showScores() {
    this.mainMenu.hide();
    this.scoresMenu.show();
  }

  toggleMute() {
    this.listeners.toggleMute.forEach(callback => callback());
    this.mainMenu.toggleMuteLabel();
    this.gameMenu.toggleMuteLabel();
    this.pauseMenu.toggleMuteLabel();
  }

  resumeGame() {
    this.pauseMenu.hide();
    // this.gameMenu.show();
    this.listeners.resume.forEach(callback => callback());
  }

  quitGame() {
      window.location.reload();
    //window.location.replace(window.location.origin + (window.location.path ? window.location.path : "") + "?start");
  }

  on(listener, callback) {
    this.listeners[listener].push(callback);
  }
}

},{"./Menus/about-menu.js":10,"./Menus/game-menu.js":12,"./Menus/level-menu.js":13,"./Menus/main-menu.js":14,"./Menus/pause-menu.js":15,"./Menus/score-menu.js":16}],19:[function(require,module,exports){
const MenuManager = require('./menu-manager.js');
const InputsManager = require('./inputs-manager.js');

module.exports = class UI {
  constructor(document, loader, gameInstance, audioInstance) {
    this.document = document;
    this.loader = loader;
    this.gameInstance = gameInstance;
    this.audio = audioInstance;
    this.menuManager = new MenuManager(this.document);
    this.inputsManager = new InputsManager(this.document);
  }

  setupUi() {
    this.setupMenus();
    this.setupInputs();

    this.document.addEventListener(
      'visibilitychange',
      () => {
        if (document['hidden'] && this.gameRunning) {
          this.menuManager.pauseGame();
        }
      },
      false
    );

    this.document.addEventListener(
      'blur',
      () => {
        if (this.gameRunning) {
          this.menuManager.pauseGame();
        }
      }
    );

    this.showProgress();
    this.loadUiAssets().then(() => {
      this.hideProgress();

      if (localStorage.getItem('muted')) {
        this.menuManager.toggleMute();
      }

      this.audio.playMenuTheme();
      this.menuManager.mainMenu.show();
      if (window.location.href.endsWith("?start")) {
        this.menuManager.startGame();
      }
    });
  }

  showProgress() {
    this.progressBar.style.display = 'block';
  }

  hideProgress() {
    this.progressBar.style.display = 'none';
  }

  loadUiAssets() {
    return this.loadAssets('manifests/ui.json');
  }

  loadGameAssets() {
    return this.loadAssets('manifests/game.json');
  }

  loadAssets(manifest) {
    return this.loader
      .setProgressCallback((loaded, progress, total) => this.updateProgress(loaded, progress, total))
      .loadManifest(manifest);
  }

  setupMenus() {
    this.menuManager.setupMenus();
    this.menuManager.on('start', (level) => this.start(level));
    this.menuManager.on('pause', () => this.pause());
    this.menuManager.on('resume', () => this.resume());
    this.menuManager.on('toggleMute', () => this.toggleMute());
    this.menuManager.on('quit', () => this.quit());
  }

  setupInputs() {
    this.inputsManager.on('keydown', (key) => this.handleKeydown(key));
    this.inputsManager.on('keyup', (key) => this.handleKeyup(key));

    this.inputsManager.on('mousedown', (button, x, y, buttons) => this.handleMousedown(button, x, y, buttons));
    this.inputsManager.on('mouseup', (button, x, y, buttons) => this.handleMouseup(button, x, y, buttons));
    this.inputsManager.on('mousemove', (x, y, buttons) => this.handleMousemove(x, y, buttons));
  }

  start(level) {
    this.loadLevel(level).then(() => {
      this.audio.stopMenuTheme();
      this.gameInstance.start();
    });
  }

  loadLevel(level) {
    return new Promise((resolve, reject) => {
      this.showProgress();
      this.loadGameAssets().then(() => {
        this.gameInstance.setCanvas(this.document.querySelector('#mainCanvas'));
        this.gameInstance.setTicker();
        this.hideProgress();
        resolve();
      });
    });
  }

  pause() {
    this.gameInstance.pause();
  }

  resume() {
    this.gameInstance.resume();
  }

  quit() {
    this.gameInstance.stop();
    this.audio.playMenuTheme();
  }

  toggleMute() {
    if (this.audio.muted) {
      this.audio.unmute();
      localStorage.removeItem('muted');
    } else {
      this.audio.mute();
      localStorage.setItem('muted', true);
    }

  }

  handleKeydown(key) {
    switch (key.toLowerCase()) {
      case 'escape':
        break;
      default:
        if (!this.gameRunning) {
          return;
        }

        this.gameInstance.handleKeydown(key);
    }
  }

  handleKeyup(key) {
    switch (key.toLowerCase()) {
      case 'escape':
        if (!this.gameStarted) {
          return;
        }

        if (this.gamePaused) {
          this.menuManager.resumeGame();
          return;
        }

        this.menuManager.pauseGame();

        break;
      default:
        if (!this.gameRunning) {
          return;
        }

        this.gameInstance.handleKeyup(key);
    }
  }

  handleMousedown(button, x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMousedown(button, x, y, buttons);
  }

  handleMouseup(button, x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMouseup(button, x, y, buttons);
  }

  handleMousemove(x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMousemove(x, y, buttons);
  }

  updateProgress(loaded, progress, total) {
    if (!this.progressBar) {
      return;
    }

    this.progressBar.style.width = `${Math.round(progress * 100)}%`;
  }

  get progressBar() {
    this._progressBar = this._progressBar || this.document.querySelector('#progressBar');

    return this._progressBar;
  }

  get gameStarted() {
    return this.gameInstance.started;
  }

  get gamePaused() {
    return this.gameStarted && this.gameInstance.paused;
  }

  get gameRunning() {
    return this.gameStarted && !this.gamePaused;
  }
}

},{"./inputs-manager.js":17,"./menu-manager.js":18}],20:[function(require,module,exports){
const Audio = require('./Game/audio.js');
const Game = require('./Game/game.js');
const Loader = require('./Loader/loader.js');
const UI = require('./UI/ui.js');

window.onload = function () {

  const debug = false;

  const loader = new Loader();
  const audio = new Audio(loader);
  const game = new Game(loader, audio, debug);
  const ui = new UI(window.document, loader, game, audio);

  ui.setupUi();

  if (debug) {
    window.loader = loader;
    window.game = game;
    window.ui = ui;
  }

    
};

},{"./Game/audio.js":1,"./Game/game.js":3,"./Loader/loader.js":9,"./UI/ui.js":19}]},{},[20]);
