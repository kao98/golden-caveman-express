Please use EditorConfig ide plug-in for consistent development environment.

NodeJS / NPM are required.

```shell
# to setup the project, install dependencies
# - browserify
# - watchify
# - beefy
npm install

# to build the bundle
npm run-script build

# to watch sources to automatically build the bundle
# you still need your own server
npm run-script watch

# to watch sources and start a live-reload server
# note: the bundle is built in live but not generated on disk.
#       you still need to build it before releasing it.
npm run-script serve
```

- `assets`: UI and Game assets like images, sounds, ...
- `css`: stylesheets for the HTML UI
- `dist`: the generated javascript bundle
- `lib`: third-party libraries (create.min.js, ...)
- `manifests`: manifests files for the preloader
- `src`: javascript sources

Everything is splitted in two parts: UI and Game.
UI referes to the GUI (the index.html home page, the menus, ...)
and to user interactions (clicks, key press, ...)

The Game is the javascript game itself.
