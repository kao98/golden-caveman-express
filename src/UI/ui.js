const MenuManager = require('./menu-manager.js');
const InputsManager = require('./inputs-manager.js');

module.exports = class UI {
  constructor(document, loader, gameInstance, audioInstance) {
    this.document = document;
    this.loader = loader;
    this.gameInstance = gameInstance;
    this.audio = audioInstance;
    this.menuManager = new MenuManager(this.document);
    this.inputsManager = new InputsManager(this.document);
  }

  setupUi() {
    this.setupMenus();
    this.setupInputs();

    this.document.addEventListener(
      'visibilitychange',
      () => {
        if (document['hidden'] && this.gameRunning) {
          this.menuManager.pauseGame();
        }
      },
      false
    );

    this.document.addEventListener(
      'blur',
      () => {
        if (this.gameRunning) {
          this.menuManager.pauseGame();
        }
      }
    );

    this.showProgress();
    this.loadUiAssets().then(() => {
      this.hideProgress();

      if (localStorage.getItem('muted')) {
        this.menuManager.toggleMute();
      }

      this.audio.playMenuTheme();
      this.menuManager.mainMenu.show();
      if (window.location.href.endsWith("?start")) {
        this.menuManager.startGame();
      }
    });
  }

  showProgress() {
    this.progressBar.style.display = 'block';
  }

  hideProgress() {
    this.progressBar.style.display = 'none';
  }

  loadUiAssets() {
    return this.loadAssets('manifests/ui.json');
  }

  loadGameAssets() {
    return this.loadAssets('manifests/game.json');
  }

  loadAssets(manifest) {
    return this.loader
      .setProgressCallback((loaded, progress, total) => this.updateProgress(loaded, progress, total))
      .loadManifest(manifest);
  }

  setupMenus() {
    this.menuManager.setupMenus();
    this.menuManager.on('start', (level) => this.start(level));
    this.menuManager.on('pause', () => this.pause());
    this.menuManager.on('resume', () => this.resume());
    this.menuManager.on('toggleMute', () => this.toggleMute());
    this.menuManager.on('quit', () => this.quit());
  }

  setupInputs() {
    this.inputsManager.on('keydown', (key) => this.handleKeydown(key));
    this.inputsManager.on('keyup', (key) => this.handleKeyup(key));

    this.inputsManager.on('mousedown', (button, x, y, buttons) => this.handleMousedown(button, x, y, buttons));
    this.inputsManager.on('mouseup', (button, x, y, buttons) => this.handleMouseup(button, x, y, buttons));
    this.inputsManager.on('mousemove', (x, y, buttons) => this.handleMousemove(x, y, buttons));
  }

  start(level) {
    this.loadLevel(level).then(() => {
      this.audio.stopMenuTheme();
      this.gameInstance.start();
    });
  }

  loadLevel(level) {
    return new Promise((resolve, reject) => {
      this.showProgress();
      this.loadGameAssets().then(() => {
        this.gameInstance.setCanvas(this.document.querySelector('#mainCanvas'));
        this.gameInstance.setTicker();
        this.hideProgress();
        resolve();
      });
    });
  }

  pause() {
    this.gameInstance.pause();
  }

  resume() {
    this.gameInstance.resume();
  }

  quit() {
    this.gameInstance.stop();
    this.audio.playMenuTheme();
  }

  toggleMute() {
    if (this.audio.muted) {
      this.audio.unmute();
      localStorage.removeItem('muted');
    } else {
      this.audio.mute();
      localStorage.setItem('muted', true);
    }

  }

  handleKeydown(key) {
    switch (key.toLowerCase()) {
      case 'escape':
        break;
      default:
        if (!this.gameRunning) {
          return;
        }

        this.gameInstance.handleKeydown(key);
    }
  }

  handleKeyup(key) {
    switch (key.toLowerCase()) {
      case 'escape':
        if (!this.gameStarted) {
          return;
        }

        if (this.gamePaused) {
          this.menuManager.resumeGame();
          return;
        }

        this.menuManager.pauseGame();

        break;
      default:
        if (!this.gameRunning) {
          return;
        }

        this.gameInstance.handleKeyup(key);
    }
  }

  handleMousedown(button, x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMousedown(button, x, y, buttons);
  }

  handleMouseup(button, x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMouseup(button, x, y, buttons);
  }

  handleMousemove(x, y, buttons) {
    if (!this.gameRunning) {
      return;
    }

    this.gameInstance.handleMousemove(x, y, buttons);
  }

  updateProgress(loaded, progress, total) {
    if (!this.progressBar) {
      return;
    }

    this.progressBar.style.width = `${Math.round(progress * 100)}%`;
  }

  get progressBar() {
    this._progressBar = this._progressBar || this.document.querySelector('#progressBar');

    return this._progressBar;
  }

  get gameStarted() {
    return this.gameInstance.started;
  }

  get gamePaused() {
    return this.gameStarted && this.gameInstance.paused;
  }

  get gameRunning() {
    return this.gameStarted && !this.gamePaused;
  }
}
