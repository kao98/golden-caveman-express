const MainMenu = require('./Menus/main-menu.js')
const AboutMenu = require('./Menus/about-menu.js')
const ScoreMenu = require('./Menus/score-menu.js')
const LevelMenu = require('./Menus/level-menu.js')
const PauseMenu = require('./Menus/pause-menu.js')
const GameMenu = require('./Menus/game-menu.js')

module.exports = class MenuManager {
  constructor(document) {
    this.document = document;

    this.mainMenu = new MainMenu(this.document);
    this.aboutMenu = new AboutMenu(this.document);
    this.scoresMenu = new ScoreMenu(this.document);
    this.levelMenu = new LevelMenu(this.document);
    this.pauseMenu = new PauseMenu(this.document);
    this.gameMenu = new GameMenu(this.document);

    this.listeners = {
      start: [],
      pause: [],
      resume: [],
      toggleMute: [],
      quit: []
    };
  }

  setupMenus() {
    this.mainMenu.on('start', () => this.selectLevel());
    this.mainMenu.on('about', () => this.showAbout());
    this.mainMenu.on('scores', () => this.showScores());
    this.mainMenu.on('toggleMute', () => this.toggleMute());

    this.aboutMenu.on('close', () => this.showMainMenu());

    this.scoresMenu.on('close', () => this.showMainMenu());

    this.levelMenu.on('cancel', () => this.showMainMenu());
    this.levelMenu.on('howto', () => this.showHowto());
    this.levelMenu.on('start', () => this.startGame());

    this.pauseMenu.on('resume', () => this.resumeGame());
    this.pauseMenu.on('quit', () => this.quitGame());
    this.pauseMenu.on('toggleMute', () => this.toggleMute());

    this.gameMenu.on('pause', () => this.pauseGame());
    this.gameMenu.on('toggleMute', () => this.toggleMute());
  }

  selectLevel() {
    this.mainMenu.hide();
    this.levelMenu.show();
  }

  showHowto() {
    window.document.querySelector('#history').style.display = 'none';
    window.document.querySelector('#howtoPlay').style.display = 'block';
    this.levelMenu.howtoButton.style.display = 'none';
  }

  startGame() {
    this.mainMenu.hide();
    this.levelMenu.hide();
    this.aboutMenu.hide();
    this.pauseMenu.hide();
    this.scoresMenu.hide();
    this.gameMenu.show();
    this.listeners.start.forEach(callback => callback());
  }

  pauseGame() {
    // this.gameMenu.hide();
    this.pauseMenu.show();
    this.listeners.pause.forEach(callback => callback());
  }

  showMainMenu() {
    this.aboutMenu.hide();
    this.scoresMenu.hide();
    this.levelMenu.hide();
    this.mainMenu.show();
  }

  showAbout() {
    this.mainMenu.hide();
    this.aboutMenu.show();
  }

  showScores() {
    this.mainMenu.hide();
    this.scoresMenu.show();
  }

  toggleMute() {
    this.listeners.toggleMute.forEach(callback => callback());
    this.mainMenu.toggleMuteLabel();
    this.gameMenu.toggleMuteLabel();
    this.pauseMenu.toggleMuteLabel();
  }

  resumeGame() {
    this.pauseMenu.hide();
    // this.gameMenu.show();
    this.listeners.resume.forEach(callback => callback());
  }

  quitGame() {
      window.location.reload();
    //window.location.replace(window.location.origin + (window.location.path ? window.location.path : "") + "?start");
  }

  on(listener, callback) {
    this.listeners[listener].push(callback);
  }
}
