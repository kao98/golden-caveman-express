module.exports = class InputsManager {
  constructor(document) {
    this.document = document;
    this.listeners = {
      keydown: [],
      keyup: [],
      touchstart: [],
      touchmove: [],
      touchend: [],
      mousemove: [],
      mousedown: [],
      mouseup: []
    };

    this.keypressed = {};

    this.setHandlers();
  }

  setHandlers() {
    this.setKeyboardHandlers();
    this.setTouchHandlers();
    this.setMouseHandlers();
  }

  setKeyboardHandlers() {
    this.document.addEventListener('keydown', (event) => this.keydownHandler(event.key));
    this.document.addEventListener('keyup', (event) => this.keyupHandler(event.key));
  }

  setTouchHandlers() {
    return;
    this.document.addEventListener('touchstart', (event) => { this.touchstartHandler(event.touches, event.changedTouches); event.preventDefault(); });
    this.document.addEventListener('touchmove', (event) => { this.touchmoveHandler(event.touches, event.changedTouches); event.preventDefault(); });
    this.document.addEventListener('touchend', (event) => { this.touchendHandler(event.touches, event.changedTouches); event.preventDefault(); });
  }

  setMouseHandlers() {
    this.document.addEventListener('mousemove', (event) => this.mousemoveHandler(event.clientX, event.clientY, event.buttons));
    this.document.addEventListener('mousedown', (event) => this.mousedownHandler(event.button, event.clientX, event.clientY, event.buttons));
    this.document.addEventListener('mouseup', (event) => this.mouseupHandler(event.button, event.clientX, event.clientY, event.buttons));
  }

  keydownHandler(key) {
    if (this.keypressed[key]) {
      return;
    }

    this.keypressed[key] = true;
    this.listeners.keydown.forEach(callback => callback(key));
  }

  keyupHandler(key) {
    delete this.keypressed[key]
    this.listeners.keyup.forEach(callback => callback(key));
  }

  touchstartHandler(touches, changedTouches) {
    this.listeners.touchstart.forEach(callback => callback(touhes, changedTouches));
  }

  touchmoveHandler(touches, changedTouches) {
    this.listeners.touchmove.forEach(callback => callback(touhes, changedTouches));
  }

  touchendHandler(touches, changedTouches) {
    this.listeners.touchend.forEach(callback => callback(touhes, changedTouches));
  }

  mousemoveHandler(x, y, buttons) {
    this.listeners.mousemove.forEach(callback => callback(x, y, buttons));
  }

  mousedownHandler(button, x, y, buttons) {
    this.listeners.mousedown.forEach(callback => callback(button, x, y, buttons));
  }

  mouseupHandler(button, x, y, buttons) {
    this.listeners.mouseup.forEach(callback => callback(button, x, y, buttons));
  }

  on(event, callback) {
    this.listeners[event].push(callback);
  }
}
