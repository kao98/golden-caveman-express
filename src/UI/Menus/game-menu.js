const BaseMenu = require('./base-menu.js')

module.exports = class GameMenu extends BaseMenu {
  get documentSelector() {
    return '#gameScreen';
  }

  get pauseButton() {
    return this.get('#pauseButton')
  }

  get muteButton() {
    return this.get('#muteGameButton');
  }

  setupMenu() {
    this.addMenuItem('pause', this.pauseButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}
