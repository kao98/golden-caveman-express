const BaseMenu = require('./base-menu.js')

module.exports = class ScoreMenu extends BaseMenu {
  get documentSelector() {
    return '#scoresScreen';
  }

  get closeButton() {
    return this.get('#closeScoresButton');
  }

  setupMenu() {
    this.addMenuItem('close', this.closeButton);
  }
}
