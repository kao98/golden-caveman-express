const BaseMenu = require('./base-menu.js')

module.exports = class ScoreMenu extends BaseMenu {
  get documentSelector() {
    return '#pauseScreen';
  }

  get resumeButton() {
    return this.get('#resumeButton');
  }

  get quitButton() {
    return this.get('#quitButton');
  }

  get muteButton() {
    return this.get('#mutePauseButton');
  }

  setupMenu() {
    this.addMenuItem('resume', this.resumeButton);
    this.addMenuItem('quit', this.quitButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}
