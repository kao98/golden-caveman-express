const BaseMenu = require('./base-menu.js')

module.exports = class AboutMenu extends BaseMenu {
  get documentSelector() {
    return '#aboutScreen';
  }

  get closeButton() {
    return this.get('#closeAboutButton');
  }

  setupMenu() {
    this.addMenuItem('close', this.closeButton);
  }
}
