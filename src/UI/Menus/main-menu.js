const BaseMenu = require('./base-menu.js')

module.exports = class MainMenu extends BaseMenu {
  get documentSelector() {
    return '#mainScreen';
  }

  get startButton() {
    return this.get('#startButton');
  }

  get aboutButton() {
    return this.get('#aboutButton');
  }

  get scoresButton() {
    return this.get('#scoresButton');
  }

  get muteButton() {
    return this.get('#muteMainButton');
  }

  setupMenu() {
    this.addMenuItem('start', this.startButton);
    this.addMenuItem('about', this.aboutButton);
    this.addMenuItem('scores', this.scoresButton);
    this.addMenuItem('toggleMute', this.muteButton);
  }

  toggleMuteLabel() {
    let label = 'Mute';

    if (this.muteButton.innerText === label) {
      label = 'Unmute';
    }

    this.muteButton.innerText = label;
  }
}
