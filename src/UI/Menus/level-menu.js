const BaseMenu = require('./base-menu.js')

module.exports = class LevelMenu extends BaseMenu {
  get documentSelector() {
    return '#levelSelectionScreen';
  }

  get howtoButton() {
    return this.get('#howtoButton')
  }

  get startButton() {
    return this.get('#startLevelButton')
  }

  get cancelButton() {
    return this.get('#cancelLevelSelectionButton');
  }

  setupMenu() {
    this.addMenuItem('start', this.startButton);
    this.addMenuItem('cancel', this.cancelButton);
    this.addMenuItem('howto', this.howtoButton);
  }
}
