module.exports = class BaseMenu {
  constructor(document) {
    this.document = document;
    this.element = document.querySelector(this.documentSelector);
    this.menuItems = {};

    this.setupMenu();
  }

  addMenuItem(name, element) {
    this.menuItems[name] = element;
  }

  get(selector) {
    this[`_${selector}`] = this[`_${selector}`] || this.document.querySelector(selector);

    return this[`_${selector}`];
  }

  show() {
    this.element.style.display = 'block';
  }

  hide() {
    this.element.style.display = 'none';
  }

  on(menuItem, callback) {
    this.menuItems[menuItem].addEventListener('click', callback);
  }
}
