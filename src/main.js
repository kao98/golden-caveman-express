const Audio = require('./Game/audio.js');
const Game = require('./Game/game.js');
const Loader = require('./Loader/loader.js');
const UI = require('./UI/ui.js');

window.onload = function () {

  const debug = false;

  const loader = new Loader();
  const audio = new Audio(loader);
  const game = new Game(loader, audio, debug);
  const ui = new UI(window.document, loader, game, audio);

  ui.setupUi();

  if (debug) {
    window.loader = loader;
    window.game = game;
    window.ui = ui;
  }

    
};
