module.exports = class Loader {
  constructor() {
    this.queue = new createjs.LoadQueue();
    this.queue.installPlugin(createjs.Sound);
  }

  setProgressCallback(callback) {
    if (this.progressListener) {
      this.queue.off('progress', this.progressListener);
    }

    if (callback) {
      this.progressListener = this.queue.on(
        'progress',
        event => callback(event.loaded, event.progress, event.total)
      );
    }

    return this;
  }

  get(id) {
    return this.queue.getResult(id);
  }

  loadManifest(manifest) {
    return new Promise((resolve, reject) => {
      this.queue.on('complete', resolve);
      this.queue.loadManifest({ src: manifest, type: 'manifest' });
    });
  }
}
