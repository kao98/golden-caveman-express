module.exports = class Audio {
  constructor(loader) {
    this.loader = loader;

    this.instances = {
      menuTheme: null,
      mainTheme: null,
      secondTheme: null,
      intro: null,
      move: null,
      cat: null,
      demon: null,
      gameOver: null,
      house: null
    }
  }

  mute() {
    createjs.Sound.muted = true;
  }

  unmute() {
    createjs.Sound.muted = false;
  }

  get muted() {
    return createjs.Sound.muted;
  }

  playMenuTheme() {
    const config = this.createConfig({ loop: -1, volume: 0.5 });

    this.instances.menuTheme = createjs.Sound.play("menu-theme", config);
  }

  stopMenuTheme() {
    createjs.Tween
      .get(this.instances.menuTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.menuTheme.stop());
  }

  playMainTheme() {

    if (this.instances.mainTheme && this.instances.mainTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.mainTheme.paused) {
        this.instances.mainTheme.paused = false;
      }
      return;
    }

    const config = this.createConfig({ startTime: 0, duration: 33000, loop: 0, volume: 0.05 });

    this.instances.mainTheme = createjs.Sound.play("main-theme", config);
    createjs.Tween
      .get(this.instances.mainTheme)
      .to({volume: 0.5}, 500)
      .wait(30500)
      .to({volume: 0}, 2000)
      .call(() => {
        if (this.instances.mainTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
          this.instances.mainTheme.stop();
          this.playMainTheme()
        }
      });
  }

  stopMainTheme() {
    if (!this.instances.mainTheme) {
      return;
    }

    createjs.Tween
      .get(this.instances.mainTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.mainTheme.stop());
  }

  playSecondTheme() {

    if (this.instances.secondTheme && this.instances.secondTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.secondTheme.paused) {
        this.instances.secondTheme.paused = false;
      }
      return;
    }

    const config = this.createConfig({ delay: 5000, startTime: 34000, duration: 31000,loop: -1, volume: 0.05 });

    this.instances.secondTheme = createjs.Sound.play("main-theme", config);
    createjs.Tween
      .get(this.instances.secondTheme)
      .to({volume: 0.5}, 500);

    // this.instances.secondTheme.on('complete', () => {
    //   if (this.instances.secondTheme.playState === createjs.Sound.PLAY_SUCCEEDED) {
    //     this.instances.secondTheme.stop();
    //     this.playSecondTheme();
    //   }
    // });
  }

  stopSecondTheme() {
    if (!this.instances.secondTheme) {
      return;
    }

    createjs.Tween
      .get(this.instances.secondTheme)
      .to({volume: 0}, 500)
      .call(() => this.instances.secondTheme.stop());
  }

  playMove() {

    if (this.instances.move && this.instances.move.playState === createjs.Sound.PLAY_SUCCEEDED) {
      if (this.instances.move.paused) {
        this.instances.move.paused = false;
      }
      return;
    }

    const config = this.createConfig({ loop: -1, volume: 0.5, startTime: 500, duration: 4000 });

    this.instances.move = createjs.Sound.play("move", config);

  }

  stopMove() {
    this.instances.move.stop();
  }

  playCat() {

    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.cat = createjs.Sound.play("cat-angry", config);

  }

  playDemon() {
    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.demon = createjs.Sound.play("demon", config);
  }

  playGameOver(delay = false) {
    const config = this.createConfig({ delay: !!delay ? 1500 : 0, loop: 0, volume: 0.8 });

    this.instances.gameOver = createjs.Sound.play("game-over", config);
  }

  playInHouse() {
    const config = this.createConfig({ loop: 0, volume: 0.8 });

    this.instances.house = createjs.Sound.play("house", config);
  }

  createConfig(options) {
    options = options || {};

    return new createjs.PlayPropsConfig().set({
      interrupt: options.interrupt || createjs.Sound.INTERRUPT_ANY,
      loop: options.loop || 0, //0: no-loop - -1: inifinite
      volume: options.volume || 0.8, //between 0 and 1
      delay: options.delay || 0, //ms
      offset: options.offset || 0, //ms
      pan: options.pan || 0, //between -1 and 1
      startTime: options.startTime || null, //ms - for audio sprites - offset to start playback and loop from
      duration: options.duration || null //ms - for audio sprites
    });
  }
}
