module.exports = class House {

    constructor(loader, stage, x, y) {
      this.stage = stage;

      this.x = x
      this.y = y;

      this.light = Math.random() < 0.5;
      this.lastChange = Date.now();

      this.item = 0;

      this.dev = false;

      // dessin de la maison
      var sp = new createjs.SpriteSheet({
        images: [loader.get('maison')],
        frames: {count: 4, width: 64, height: 64},
        animations: {
          eteint: [0, 0],
          allume: [1, 3, undefined, 0.1]
        }
      });
      this.shape = new createjs.Sprite(sp, this.light ? "allume" : "eteint");

      // this.shapeFillCommand = this.shape.graphics.beginFill(this.light ? "yellow" : "gray").command;
      // this.shape.graphics.drawRect(-this.width / 2, -this.height/2, this.width, this.height);
      // this.shape.graphics.endFill();
      // // positionnement initial de la maison
      this.stage.addChild(this.shape);

      this.shape.scaleX = this.shape.scaleY = 2;
      this.width = this.height = 64 * this.shape.scaleY;

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width * 0.1, this.height * 0.4, this.width*0.8, this.height * 0.6);
      this.collisionBox.graphics.endStroke();
      stage.addChild(this.collisionBox);

    }

    get bounds() {
      return {x: this.x + this.width * 0.1, y: this.y + this.height * 0.4, w: this.width, h: this.height * 0.6 };
    }

    update() {
      this.collisionBox.visible = this.dev;
      if (Date.now() - this.lastChange > 3000 && Math.random() < 0.001) {
        this.light = !this.light;
        this.lastChange = Date.now();
        this.shape.gotoAndPlay(this.light ? "allume" : "eteint");
      }
    }

}
