module.exports = class Home {

    constructor(loader, stage, x, y) {
      this.loader = loader;
      this.stage = stage;

      this.x = x;
      this.y = y;
      this.width = 256;
      this.height = 256;

      this.dev = false;

      // dessin de la maison
      // this.bitmap = new createjs.Bitmap(this.loader.get("tileset-graveyard"));
      this.bitmap = new createjs.Bitmap(this.loader.get("tileset-graveyard"));
      this.bitmap.sourceRect = new createjs.Rectangle(256,0,128,128);
      this.stage.addChild(this.bitmap);

      this.container = new createjs.Container();
      this.container.y = 10;
      this.container.width = stage.canvas.width / 3;
      this.container.regX = this.container.width;
      this.container.x = stage.canvas.width - 10;

      this.collectedItems = [];
      this.itemsToCollect = [1, 2, 3, 4, 5, 6];

      var sp = new createjs.SpriteSheet({
        images: [loader.get("objects")],
        frames: { count: 7, width: 32, height: 32 },
        animations: {
          all: [0, 6]
        }
      });
      this.sprites = [
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp),
        new createjs.Sprite(sp)
      ];
      stage.addChild(this.container);

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width*0.05, this.height*0.12, this.width*0.25, this.height*0.3);
      this.collisionBox.graphics.endStroke();
      stage.addChild(this.collisionBox);
    }

    reset() {
      this.collectedItems = [];
      this.itemsToCollect = [1, 2, 3, 4, 5, 6];
      this.update();
    }

    get bounds() {
      return { x: this.x+ this.width*0.05, y: this.y + this.height*0.12, w: this.width*0.25, h: this.height*0.3 };
    }

    update() {
      this.container.removeAllChildren();
      this.itemsToCollect.forEach((item, i, a) => {
        this.container.addChild(this.sprites[item]);
        this.sprites[item].y = 0;
        this.sprites[item].gotoAndStop(item);
        this.sprites[item].x = this.container.width - (a.length - i) * 32;
      });
      this.collisionBox.visible = this.dev
    }

    putItem(item) {
      var index = this.itemsToCollect.indexOf(item);
      var exists = (index >= 0);
      if (exists) {
        this.collectedItems.push(item);
        this.itemsToCollect.splice(index, 1);
        this.update();
      }
      return exists;
    }

}
