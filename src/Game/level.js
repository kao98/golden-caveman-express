const Map = require('./map.js');
const House = require('./house.js');
const Home = require('./home.js');
const Enemy = require('./enemy.js');

module.exports = class Level {

  constructor(loader, stage, player, audio) {
    this.loader = loader;
    this.stage = stage;
    this.player = player;
    this.audio = audio;
    this.over = false;
    this.map = new Map(this.loader, this.stage, this.player);

    // placement du joueur
    this.player.x = this.map.width / 2;
    this.player.y = this.map.height / 2;
    this.stage.update();
    // maisons
    this.houses = [];
    this.map.houses.forEach((pos, i) => {
      this.houses.push(new House(this.loader, this.stage, pos.x, pos.y));
      // this.houses[i].light = Math.random() < 0.5;
      if (i > 0 && i < 7) {
        this.houses[i].item = i;
      }
    });
    // sa maison
    this.home = new Home(this.loader, this.stage, this.map.width * 0.5 - 32, this.map.height * 0.5 - 64);
    // ennemis
    this.enemies = [];
    this.map.mobs.forEach((pos, i) => {
      this.enemies.push(new Enemy(this.loader, this.stage, pos.x, pos.y));
    });
    // ajout du joueur
    this.player.addHud();


    this.startLabel = new createjs.Text("Press [Space] key to start running", "normal 18px Arial", "#FFF");
    this.startLabel.textAlign = 'center';
    this.startLabel.x = 480;
    this.startLabel.y = 500;
    this.startLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.startLabel.filters = [this.startLabelFilter];

    createjs.Tween.get(this.startLabelFilter, { loop: true })
      .to({redMultiplier: 0, alphaMultiplier: 0.8 }, 1000)
      .to({redMultiplier: 1, alphaMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.startLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.startLabel.cache(-this.startLabel.getMeasuredWidth()/2, 0, this.startLabel.getMeasuredWidth(), this.startLabel.getMeasuredHeight());
    this.stage.addChild(this.startLabel);

    this.overLabel = new createjs.Text("Game Over :'[", "normal 24px Arial", "#FFF");
    this.overLabel.textAlign = 'center';
    this.overLabel.x = 480;
    this.overLabel.y = 60;
    this.overLabel.visible = false;
    this.overLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.overLabel.filters = [this.overLabelFilter];

    createjs.Tween.get(this.overLabelFilter, { loop: true })
      .to({greenMultiplier: 0, blueMultiplier: 0 }, 1000)
      .to({greenMultiplier: 1, blueMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.overLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.overLabel.cache(-this.overLabel.getMeasuredWidth()/2, 0, this.overLabel.getMeasuredWidth(), this.overLabel.getMeasuredHeight());
    this.stage.addChild(this.overLabel);


    this.victoryLabel = new createjs.Text("You win! Well done, Yuri is Happy!", "normal 24px Arial", "#FFF");
    this.victoryLabel.textAlign = 'center';
    this.victoryLabel.x = 480;
    this.victoryLabel.y = 60;
    this.victoryLabel.visible = false;
    this.victoryLabelFilter = new createjs.ColorFilter(1, 1, 1, 1); //green&blue = 0, only red and alpha stay
    this.victoryLabel.filters = [this.victoryLabelFilter];

    createjs.Tween.get(this.victoryLabelFilter, { loop: true })
      .to({redMultiplier: 0, blueMultiplier: 0 }, 1000)
      .to({redMultiplier: 1, blueMultiplier: 1 }, 1000);

//     createjs.Tween.get(this.victoryLabel, { loop: true })
//       .to({ color: "#FFF" }, 1000);
    this.victoryLabel.cache(-this.victoryLabel.getMeasuredWidth()/2, 0, this.victoryLabel.getMeasuredWidth(), this.victoryLabel.getMeasuredHeight());
    this.stage.addChild(this.victoryLabel);


  }

  stopGame() {
    this.player.running = false;
    this.startLabel.visible = true;
    this.audio.stopMove();
  }

  resumeGame() {
    this.player.running = true;
    this.startLabel.visible = false;
    this.audio.playMove();
  }

  update() {
    this.player.update(this.map);
    this.map.update();
    // update house position w.r.t. player
    this.houses.forEach(function(house) {
      house.update();
      var xy = this.setCoordsInDisplayForEntity(house);
      house.shape.x = house.collisionBox.x = xy.x;
      house.shape.y = house.collisionBox.y = xy.y;
    }.bind(this));
    this.enemies.forEach(function(enemy) {
      enemy.update();
      var xy = this.setCoordsInDisplayForEntity(enemy);
      enemy.shape.x = enemy.collisionBox.x = xy.x;
      enemy.shape.y = enemy.collisionBox.y = xy.y;
    }.bind(this));

    this.home.update();
    var homexy = this.setCoordsInDisplayForEntity(this.home);
    this.home.bitmap.x = this.home.collisionBox.x = homexy.x;
    this.home.bitmap.y = this.home.collisionBox.y = homexy.y;


    this.startLabel.updateCache();
    if (this.over) {
      this.overLabel.visible = true;
      this.overLabel.updateCache();
    }

    if (!this.player.running || this.player.energy == 0) {
      if (this.player.energy == 0 && !this.over) {
        this.audio.stopMainTheme();
        this.audio.stopSecondTheme();
        this.audio.stopMove();
        this.audio.playGameOver();
        this.over = true;
      }
      return;
    }
    if (this.home.itemsToCollect.length == 0) {
      this.player.running = false;
      this.audio.stopMainTheme();
      this.audio.stopSecondTheme();
      this.audio.stopMove();
      this.victoryLabel.visible = true;
      this.victoryLabel.updateCache();
    }
    if (this.intersects(this.player, this.home)) {
      // rentre dans la tombe...
      if (this.player.item != 0) {
        if (this.home.putItem(this.player.item)) {
          this.player.increaseEnergy();
        }
        this.player.item = 0;
        this.audio.stopSecondTheme();
        this.audio.playMainTheme();
      }
      this.player.resetEnergy();
      return;
    }

    for (var i = 0; i < this.houses.length; i++) {
      if (this.intersects(this.houses[i], this.player)) {
        if (this.houses[i].light) {
          // gameover
          this.player.energy = 0;
          this.player.update();
        }
        else {
          if (this.houses[i].item > 0 && this.player.item == 0) {
            this.player.item = this.houses[i].item;
            this.houses[i].item = 0;
            this.audio.stopMainTheme();
            this.audio.playInHouse();
            this.audio.playSecondTheme();
          }
        }
      }
    }

    // enemies
    if (this.player.isDashing()) {
      return;
    }
    this.enemies.forEach((enemy, i) => {
      if (this.intersects(enemy, this.player)) {
        // gameover
        this.player.energy = 0;
        enemy.vec.x = enemy.vec.y = 0;
        this.audio.stopMove();
        this.audio.stopMainTheme();
        this.audio.stopSecondTheme();

        if (enemy.type == 0) {
          enemy.shape.gotoAndStop("attack");
          enemy.x = this.player.x - this.player.width / 2;
          enemy.y = this.player.y - this.player.height * 0.7;
          this.audio.playCat();
        } else {
          this.audio.playDemon();
        }

        this.audio.playGameOver(true);
        this.over = true;

        return;
      }
      if (enemy.isClosedTo(this.player)) {
        let dist = Math.sqrt((enemy.x-this.player.x)*(enemy.x-this.player.x)+(enemy.y-this.player.y)*(enemy.y-this.player.y));
        enemy.vec.x = (this.player.x - enemy.x) / dist;
        enemy.vec.y = (this.player.y - enemy.y) / dist;
      }
      else {
        enemy.vec.x = enemy.vec.y = 0;
      }
    });

  }

  intersects(rect1, rect2) {
      if ( rect1.bounds.x >= rect2.bounds.x + rect2.bounds.w ||  // rect1 à droite de rect2
           rect1.bounds.x + rect1.bounds.w <= rect2.bounds.x ||  // rect1 à gauche de rect2 
           rect1.bounds.y >= rect2.bounds.y + rect2.bounds.h ||  // rect1 au dessous de rect2
           rect1.bounds.y + rect1.bounds.h <= rect2.bounds.y )  //  rect1 au dessus de rect2
          return false;
      return true;
  }

  // assume entity has .x and .y properties for positions in world
  setCoordsInDisplayForEntity(entity) {
    var deltaX = entity.x - this.player.x;
    var deltaY = entity.y - this.player.y;
    // dépasse au dessus :    // rajouter la taille du bloc
    if (this.player.y < this.player.shape.y) {
      var debord = this.player.shape.y - this.player.y;
      if (entity.y + entity.height >= this.map.height - debord) {
      deltaY -= this.map.height;
      }
    }
    // dépasse en dessous :
    else if (this.player.y > this.map.height - this.player.shape.y) {
      var debord = this.player.shape.y - (this.map.height - this.player.y);
      if (entity.y - entity.height <= debord) {
        deltaY += this.map.height;
      }
    }

    // dépasse à gauche
    if (this.player.x < this.player.shape.x) {
      var debord = this.player.shape.x - this.player.x;
      if (entity.x + entity.width >= this.map.width - debord) {
        deltaX -= this.map.width;
      }
    }
    // dépasse à droite
    else if (this.player.x > this.map.width - this.player.shape.x) {
      var debord = this.player.x - (this.map.width - this.player.shape.x);
      if (entity.x - entity.width <= debord) {
        deltaX += this.map.width;
      }
    }

    return {x: (deltaX + this.stage.canvas.width / 2), y: (deltaY + this.stage.canvas.height / 2)};
  }
}
