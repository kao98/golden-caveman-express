module.exports = class Map {

  constructor(loader, stage, player) {
    this.stage = stage;
    this.loader = loader;

    this.map = new createjs.Container();
    this.map.x = 0;
    this.map.y = 0;

    this.stage.addChild(this.map);

    this.tileW = 32;
    this.tileH = 32;

    this.player = player;

    this.nbMobs = 42;
    this.nbHouse = 10;
    this.nbItemGraveyard = 20;
    this.nbItemMap = 400;
    this.nbFence = 100;
    this.nbItemHouse = 7;

    var nbCaseX = Math.floor(Math.random() * (90 - 30)) + 180;
    var nbCaseY = Math.floor(Math.random() * (48 - 12)) + 100;
    this.worldGeneration = this.generateMap(nbCaseX, nbCaseY);
    this.matrix = [];
    this.houses = [];
    this.mobs = [];
    //Génération des sols
    //Génération du fond de map
    for (var i=0; i < nbCaseY; i++) {   
      this.matrix.push([]);
      for (var j=0; j < nbCaseX; j++) {
        this.matrix[i].push((this.worldGeneration[i*nbCaseX+j] ? "1" : "2"));
      }
    }

    //Enleve cases seules
    for (var i=1; i < nbCaseY-1; i++) {
      for (var j=1; j < nbCaseX-1; j++) {
        this.matrix[i].push((this.worldGeneration[i*nbCaseX+j] ? "1" : "2"));
      }
    }

    //Génération de la tombe
    let midX = Math.floor(nbCaseX/2);
    let midY = Math.floor(nbCaseY/2);
    for (var i=midY-4; i<midY+4; i++) {
      for (var j=midX-4; j<midX+4; j++) {
        this.matrix[i][j]="1";
      }
    }

    //Exclusion autour du mausolé
    for (var i=midY-3; i<midY+3; i++) {
      for (var j=midX-3; j<midX+3; j++) {
        this.matrix[i][j] += "0";
      }
    }

    //Ajout d'objets de décoration à proximité du cimetiere
    for (var i=0; i<this.nbItemGraveyard ; i++) {
      let randX = midX - 5 + Math.floor(Math.random() * 10);
      let randY = midY - 5 + Math.floor(Math.random() * 10);
      if(this.matrix[randY][randX].length < 2){
        this.matrix[randY][randX] += Math.floor(Math.random() * 4) + 1;
      }
    } 

    //Exclusion autour du cimetiere
    for (var i=midY-5; i<midY+5; i++) {
      for (var j=midX-5; j<midX+5; j++) {
        if(this.matrix[i][j].length < 2){
          this.matrix[i][j] += "0";
        }
      }
    }

    //Ajout de pierres
    for (var i=midY-4; i<midY+3; i++) {
      for (var j=midX-3; j<midX+3; j++) {
        switch (Math.floor(Math.random() * 8)) {
          case 0:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'V';
            break;
          case 1:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'W';
            break;
          case 2:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'X';
            break;
          case 3:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'Y';
            break;
          case 4:
            this.matrix[i][j] = this.matrix[i][j].substr(0, 1) + 'Z';
            break;
        }
      }
    }

    //Ajout d'objets dans la map
    for (var i=0; i<this.nbItemMap ; i++) {
      let randX = Math.floor(Math.random() * this.matrix[0].length);
      let randY = Math.floor(Math.random() * this.matrix.length);
      if(this.matrix[randY][randX].length < 2){
        let alea = Math.floor(Math.random() * 5) + 5;
        if(alea == 9){
          if(this.matrix[randY][randX].charAt(0) == 2 ){
            this.matrix[randY][randX] += alea;
          }
        }else{
          this.matrix[randY][randX] += alea;
        }
      }
    } 

    //Aucun objet de décorations
    for (var i=0; i < nbCaseY; i++) {
      for (var j=0; j < nbCaseX; j++) {
        if(this.matrix[i][j].length < 2){
          this.matrix[i][j] += "0";
        }
      }
    }

    //Ajout des barrières
    this.matrix[midY+5][midX+2] = this.matrix[midY+5][midX+2].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-2] = this.matrix[midY+5][midX-2].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-1] = this.matrix[midY+5][midX-1].substr(0, 1) + 'T';
    this.matrix[midY+5][midX-3] = this.matrix[midY+5][midX-3].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-2] = this.matrix[midY-5][midX-2].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-1] = this.matrix[midY-5][midX-1].substr(0, 1) + 'T';
    this.matrix[midY-5][midX-3] = this.matrix[midY-5][midX-3].substr(0, 1) + 'T';

    //Calcul des positions des maisons
    for(var i=0; i<this.nbHouse; i++){
      let Coordx = 0;
      let Coordy = 0;
      do{
        Coordx = Math.floor(Math.random() * this.matrix[0].length);
        Coordy = Math.floor(Math.random() * this.matrix.length);
      }while(!this.canBuild(Coordx,Coordy));
      //Push tableau des maisons
      this.matrix[Coordy][Coordx] = this.matrix[Coordy][Coordx].substr(0, 1) + 'H';
      Coordx *= this.tileW 
      Coordy *= this.tileH
      this.houses.push({x:Coordx, y:Coordy});
    }

    //Ajout dd'objets autour de maisons
    this.houses.forEach((house) => {
      for (var i=0; i<this.nbItemHouse ; i++) {
        let randX = (house.x/this.tileW ) - 4 + Math.floor(Math.random() * 8);
        let randY = (house.y/this.tileH )- 4 + Math.floor(Math.random() * 8);
        if(this.matrix[randY][randX].charAt(1) != 'H'){
          switch (Math.floor(Math.random() * 4)) {
            case 0:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'A';
              break;
            case 1:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'B';
              break;
            case 2:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'C';
              break;
            case 3:
              this.matrix[randY][randX] = this.matrix[randY][randX].substr(0, 1) + 'D';
              break;
          }
        }
      } 

      this.tilesCorrespondance = {
        1: new createjs.Rectangle(0, 96, 64, 64),
        2: new createjs.Rectangle(64, 96, 64, 64),
        3: new createjs.Rectangle(96, 32, 64, 64),
        4: new createjs.Rectangle(32, 64, 64, 64),
        5: new createjs.Rectangle(0, 0, 64, 64),
        6: new createjs.Rectangle(32, 0, 64, 64),
        7: new createjs.Rectangle(128, 32, 64, 64),
        8: new createjs.Rectangle(180, 3, 32, 32),
        9: new createjs.Rectangle(159, 193, 32, 32),
        A: new createjs.Rectangle(0, 192, 64, 64),
        B: new createjs.Rectangle(32, 192, 64, 64),
        C: new createjs.Rectangle(32, 128, 64, 64),
        D: new createjs.Rectangle(64, 128, 64, 64),
        T: new createjs.Rectangle(32, 320, 256, 256),
        V: new createjs.Rectangle(0, 160, 64, 64),
        W: new createjs.Rectangle(0, 192, 64, 64),
        X: new createjs.Rectangle(32, 192, 64, 64),
        Y: new createjs.Rectangle(64, 160, 64, 64),
        Z: new createjs.Rectangle(32, 160, 64, 64)
      }

    })


    //Calcul des positions des mobs dans le champs
    for(var i=0; i<this.nbMobs; i++){
      let Coordx = 0;
      let Coordy = 0;
      do{
        Coordx = Math.floor(Math.random() * this.matrix[0].length);
        Coordy = Math.floor(Math.random() * this.matrix.length);
      }while(!this.canSpawn(Coordx,Coordy));
      //Push tableau des mobs
      this.matrix[Coordy][Coordx] = this.matrix[Coordy][Coordx].substr(0, 1) + 'M';
      Coordx *= this.tileW 
      Coordy *= this.tileH
      this.mobs.push({x:Coordx, y:Coordy});
    } 
  }

  get width() {
    return this.matrix[0].length*this.tileW;
  }

  get height() {
    return this.matrix.length*this.tileH;
  } 

  update() {
    this.map.removeAllChildren();

    let longueur = this.tileW * this.matrix[0].length;
    if (this.player.x > longueur) {
      this.player.x -= longueur;
    }
    else if (this.player.x < 0) {
      this.player.x = longueur + this.player.x;
    }

    let hauteur = this.tileH * this.matrix.length;
    if (this.player.y > hauteur) {
      this.player.y -= hauteur;
    }
    else if (this.player.y < 0) {
      this.player.y = hauteur + this.player.y;
    }


    var player1_tile_x = Math.floor(this.player.x / this.tileW);
    var player1_tile_y = Math.floor(this.player.y / this.tileH);

    let start_x = player1_tile_x - Math.floor(this.stage.canvas.width / 2 / this.tileW) - 1;
    let end_x = player1_tile_x + Math.floor(this.stage.canvas.width / 2 / this.tileW) + 2;
    let start_y = player1_tile_y - Math.floor(this.stage.canvas.height / 2 / this.tileH) - 1;
    let end_y = player1_tile_y + Math.floor(this.stage.canvas.height / 2 / this.tileH) + 2;

    for (var i = start_x; i < end_x; ++i) {
      for (var j = start_y; j < end_y; ++j) {
        var myI = (i < 0) ? this.matrix[0].length + i : i;
        var myJ = (j < 0) ? this.matrix.length + j : j;
        myI = (myI >= this.matrix[0].length) ? myI - this.matrix[0].length: myI;
        myJ = (myJ >= this.matrix.length) ? myJ - this.matrix.length: myJ;

        var x = (this.stage.canvas.width / 2) - ((this.player.x - ((i) * this.tileW)));
        var y = (this.stage.canvas.height / 2) - ((this.player.y - ((j) * this.tileH)));
        var bitmap = null;
        try {
          //Affichage sol
          switch (this.matrix[myJ][myI].charAt(0)) {
            case "1":
              var color = "#65845c";
              break;
            case "2":
              var color = "#3a5941";
              break;
          }

          if (this.tilesCorrespondance[this.matrix[myJ][myI].charAt(1)]) {
            bitmap = new createjs.Bitmap(this.loader.get('tileset-graveyard'));
            bitmap.sourceRect = this.tilesCorrespondance[this.matrix[myJ][myI].charAt(1)];
            bitmap.x = x;
            bitmap.y = y;
          }
        } catch (error) {
          debugger;
        }

        var rect = new createjs.Shape();
        rect.graphics.beginFill(color);
        rect.graphics.drawRect(0, 0, this.tileW+1, this.tileH+1);
        rect.graphics.endFill();
        rect.x = x;
        rect.y = y;
        this.map.addChild(rect);
          
        if (bitmap) {
          this.map.addChild(bitmap);
        }
      }
    }
  }

  generateMap(nbCaseX, nbCaseY){
    let tileArray = new Array();
    let probabilityModifier = 0;
    let landMassAmount=1; // scale of 1 to 5
    let landMassSize=3; // scale of 1 to 5
    let rndm;
    let probability = 15 + landMassAmount;
    let conformity;

    for (let i = 0; i < nbCaseX*nbCaseY; i++) {
        if (i>(nbCaseX*2)+2){
          // Conform the tile upwards and to the left to its surroundings
          conformity =
            (tileArray[i-nbCaseX-1]==(tileArray[i-(nbCaseX*2)-1]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-nbCaseX]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-1]))+
            (tileArray[i-nbCaseX-1]==(tileArray[i-nbCaseX-2]));
          if (conformity<2){
            tileArray[i-nbCaseX-1]=!tileArray[i-nbCaseX-1];
          }
        }
        // get the probability of what type of tile this would be based on its surroundings
        probabilityModifier = (tileArray[i-1]+tileArray[i-nbCaseX]+tileArray[i-nbCaseX+1])*(19+(landMassSize*1.4));
        rndm=(Math.random()*101);
        tileArray[i]=(rndm<(probability+probabilityModifier));
    }
    return tileArray; 
  }

  canBuild(x,y){
    let partX = this.matrix[0].length / 8;
    let partY = this.matrix.length / 8;
    if (x < partX || x > 7 * partX || y < partY || y > 7 * partY) {
      return false;
    }
    if (x > 2 * partX && x < 6 * partX && y > 2 * partY && y < 6 * partY) {
      return false;
    }
    let
      lowx = Math.max(x-4, 0),
      highx = Math.min(x+4, this.matrix[0].length),
      lowy = Math.max(y-4, 0),
      highy = Math.min(y+4, this.matrix.length)
    ;
    for (var i=lowy; i < highy; i++) {
      for (var j=lowx; j < highx; j++) {
        if(this.matrix[i][j].charAt(1) != "0"){
          return false;
        }
      }
    }
    return true;
  }

  canSpawn(x,y){
    let partX = this.matrix[0].length / 8;
    let partY = this.matrix.length / 8;
    if (x < partX || x > 7 * partX || y < partY || y > 7 * partY) {
      return false;
    }
    if (x > 3 * partX && x < 5 * partX && y > 3 * partY && y < 5 * partY) {
      return false;
    }
    let
      lowx = Math.max(x-4, 0),
      highx = Math.min(x+4, this.matrix[0].length),
      lowy = Math.max(y-4, 0),
      highy = Math.min(y+4, this.matrix.length)
    ;
    for (var i=lowy; i < highy; i++) {
      for (var j=lowx; j < highx; j++) {
        if(this.matrix[i][j].charAt(1) != "0"){
          return false;
        }
      }
    }
    return true;
  }
  
}




