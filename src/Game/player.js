module.exports = class Player {

  constructor(loader, stage) {
      this.loader = loader;

      this.x = stage.canvas.width/2;
      this.y = stage.canvas.height/2;
      this.vec = {x: 0, y: 0};
      this.nextVec = {x: 0, y: -1};
      this.stage = stage;

      this.initSpeed = 4;
      this.speed = this.initSpeed;

      this.initEnergy = 100;
      this.maxEnergy = this.initEnergy;
      this.energy = this.initEnergy;

      this.initDeltaNRJ = 0.01;
      this.deltaNRJ = this.initDeltaNRJ;

      this.running = false;
      this.dev = false;

      // item portée par le personnage (0: aucun, 1: item1, 2: item2, etc.)
      this.item = 0;

      // spritesheets du personnage
      var data = {
        images: [
          this.loader.get("ghost_go_down"),
          this.loader.get("ghost_go_up"),
          this.loader.get("ghost_go_left"),
          this.loader.get("ghost_go_right"),
          this.loader.get("ghost_dash_down"),
          this.loader.get("ghost_dash_up"),
          this.loader.get("ghost_dash_left"),
          this.loader.get("ghost_dash_right"),
          this.loader.get("ghost_dash_right_down"),
          this.loader.get("ghost_dash_right_up"),
          this.loader.get("ghost_dash_left_down"),
          this.loader.get("ghost_dash_left_up"),
          this.loader.get("ghost_go_in_house")
        ],
        frames: { count: 73, width: 32, height: 32 },
        animations: {
          go_down: [0, 5, undefined, 0.42 ],
          go_up: [6, 11, undefined, 0.42 ],
          go_left: [12, 17, undefined, 0.42 ],
          go_right: [18, 23, undefined, 0.42 ],
          dash_down: [24, 27, "dash_down_wait", 0.42 ],
          dash_down_wait: [27, 27, undefined, 0.42],
          dash_down_end: [27, 29, "go_down", 0.15],
          dash_up: [30, 33, "dash_up_wait", 0.42 ],
          dash_up_wait: [33, 33, undefined, 0.42 ],
          dash_up_end: [33, 35, "go_up", 0.15 ],
          dash_left: [36, 39, "dash_left_wait", 0.42 ],
          dash_left_wait: [39, 39, undefined, 0.42 ],
          dash_left_end: [39, 41, "go_left", 0.15 ],
          dash_right: [42, 45, "dash_right_wait", 0.42 ],
          dash_right_wait: [45, 45, undefined, 0.42 ],
          dash_right_end: [45, 47, "go_right", 0.15 ],
          
          dash_right_down: [48, 51, "dash_right_down_wait", 0.42],
          dash_right_down_wait: [51, 51, undefined, 0.42],
          dash_right_down_end: [51, 53, "go_right", 0.42],

          dash_right_up: [54, 57, "dash_right_up_wait", 0.42 ],
          dash_right_up_wait: [57, 57, undefined, 0.42 ],
          dash_right_up_end: [57, 59, "go_right", 0.42 ],

          dash_left_down: [60, 63, "dash_left_down_wait", 0.42 ],
          dash_left_down_wait: [63, 63, undefined, 0.42 ],
          dash_left_down_end: [63, 65, "go_left", 0.42 ],

          dash_left_up: [66, 69, "dash_left_up_wait", 0.42 ],
          dash_left_up_wait: [69, 69, undefined, 0.42 ],
          dash_left_up_end: [69, 71, "go_left", 0.42 ],

          go_in_house: [72]
        }
      };
      var ghostSpriteSheet = new createjs.SpriteSheet(data);
      this.currentAnimation = "go_down";
      this.shape = new createjs.Sprite(ghostSpriteSheet, this.currentAnimation);

      //this.shape = new createjs.Bitmap("./assets/game/ghost.png");
      this.shape.regX = 16;
      this.shape.regY = 16;
      this.shape.scaleX = 1.4;
      this.shape.scaleY = 1.4;
      //this.stage.addChild(this.shape);

      this.width = 32 * this.shape.scaleX;
      this.height = 32 * this.shape.scaleY;

      // éventuel item du personnage
      var sp = new createjs.SpriteSheet({
        images: [loader.get("objects")],
        frames: { count: 7, width: 32, height: 32 },
        animations: {
          all: [0, 6]
        }
      });
      this.carriedItem = new createjs.Sprite(sp);
      this.carriedItem.gotoAndStop(this.item);
      this.carriedItem.x = this.stage.canvas.width / 2 + 10;
      this.carriedItem.y = this.stage.canvas.height / 2 - this.height;
      this.carriedItem.scaleX = 0.8;
      this.carriedItem.scaleY = 0.8;

      // barre de vie du personnage
      this.barre = new createjs.Shape();
      this.barre.width = stage.canvas.width / 3;
      this.barre.height = 20;
      this.barre.x = 10;
      this.barre.y = 10;
      this.barre.graphics.setStrokeStyle(1);
      this.barre.graphics.beginStroke("#000000");
      this.barre.graphics.drawRect(-1, -1, this.barre.width + 2, this.barre.height + 2);
      this.barre.graphics.endStroke();
      // jauge
      this.jauge = new createjs.Shape();
      this.jaugeFillCommand = this.jauge.graphics.beginFill("hsl(" + (130 * this.energy/100) + ", 100%, 50%)").command;
      this.jauge.graphics.drawRect(0, 0, this.barre.width, this.barre.height);
      this.jauge.graphics.endFill();
      this.jauge.x = 10;
      this.jauge.y = 10;
      // fleche
      this.fleche = new createjs.Bitmap(this.loader.get("fleche"));
      this.fleche.x = 0;
      this.fleche.y = 0;
      this.fleche.regX = 16;
      this.fleche.regY = 16;
      this.fleche.visible = false;

      // collision shape
      this.collisionBox = new createjs.Shape();
      this.collisionBox.graphics.beginStroke("red");
      this.collisionBox.graphics.drawRect(this.width *0.25, this.height*0.25, this.width*0.5, this.height*0.5);
      this.collisionBox.graphics.endStroke();

      // positionnement du personnage --> toujours au milieu de l'écran
      this.shape.x = this.stage.canvas.width / 2;
      this.collisionBox.x = this.stage.canvas.width / 2 - this.width / 2 | 0;
      this.shape.y = this.stage.canvas.height / 2;
      this.collisionBox.y = this.stage.canvas.height / 2 - this.height / 2 | 0;

  }

  addHud() {
    this.stage.addChild(this.shape);
    this.stage.addChild(this.collisionBox);
    this.stage.addChild(this.carriedItem);
    this.stage.addChild(this.fleche);
    this.stage.addChild(this.barre);
    this.stage.addChild(this.jauge);
  }

  get bounds() {
    return { x: this.x - this.width * 0.25, y: this.y - this.height * 0.25, w: this.width * 0.5, h: this.height * 0.5 };
  }

  // recalcul du vecteur de déplacement
  pointTo(x, y) {
      var rect = this.stage.canvas.getBoundingClientRect();
      x = x - rect.left;
      y = y - rect.top;
      let x0 = this.stage.canvas.clientWidth / 2;
      let y0 = this.stage.canvas.clientHeight / 2;
      let distX = x-x0;
      let distY = y-y0;
      let dist = Math.sqrt(distX*distX+distY*distY);
      this.nextVec = { x: distX / dist, y: distY / dist };
  }

  increaseSpeed() {
      if (!this.running) return;
    this.speed = this.initSpeed * 3;
    this.deltaNRJ = this.initDeltaNRJ * 3;
    var cost = this.computeNextCostume();
    this.lastDash = "dash" + cost.substring(2);
    switch (this.lastDash) {
      case "dash_right":
        if (this.vec.y > 0.3) {
          this.lastDash = "dash_right_down";
        }
        else if (this.vec.y < -0.3) {
          this.lastDash = "dash_right_up";
        }
        break;
      case "dash_left": 
        if (this.vec.y > 0.3) {
          this.lastDash = "dash_left_down";
        }
        else if (this.vec.y < -0.3) {
          this.lastDash = "dash_left_up";
        }
        break;
      case "dash_up": 
        if (this.vec.x > 0.3) {
          this.lastDash = "dash_right_up";
        }
        else if (this.vec.x < -0.3) {
          this.lastDash = "dash_left_up";
        }
        break;
      case "dash_down": 
        if (this.vec.x > 0.3) {
          this.lastDash = "dash_right_down";
        }
        else if (this.vec.x < -0.3) {
          this.lastDash = "dash_left_down";
        }
        break;
    }
    this.shape.gotoAndPlay(this.lastDash);
  }



  decreaseSpeed() {
    var cost = this.computeNextCostume();
    this.currentAnimation = cost;
    this.shape.gotoAndPlay(this.lastDash + "_end");  
    this.speed = this.initSpeed;
    this.deltaNRJ = this.initDeltaNRJ;
  }

  isDashing() {
    return this.speed != this.initSpeed;
  }

  reset(map) {
    this.speed = this.initSpeed;
    this.energy = this.initEnergy;
    if (map) {
      this.x = map.width / 2;
      this.y = map.height / 2;
    }
  }

  increaseEnergy() {
    this.maxEnergy *= 1.2;
  }
  resetEnergy() {
    this.energy = this.maxEnergy;
  }

  update(map) {
    this.collisionBox.visible = this.dev;
    if (!this.running) {
      return;
    }
    if (this.energy > 0) {
      // déplacement du personnage
      if (! this.isDashing()) {
        this.vec.x = this.nextVec.x;
        this.vec.y = this.nextVec.y;
      }
      this.x += this.vec.x * this.speed;
      this.y += this.vec.y * this.speed;
      // costume du personnage
      if (this.speed == this.initSpeed) {
        var nextCostume = this.computeNextCostume();
        if (nextCostume != this.currentAnimation) {
          this.currentAnimation = nextCostume;
          this.shape.gotoAndPlay(this.currentAnimation);
        }
      }
      // consommation d'énergie
      var delta = (this.dev ? this.initDeltaNRJ : this.deltaNRJ);

      this.energy -= (this.speed * delta * (this.dev ? 0.5 : 1));
      if (this.energy < 0) {
        this.energy = 0;
      }
      // fleche
      var x0 = map.width / 2;
      var y0 = map.height / 2;
      var dist = Math.sqrt((this.x-x0)*(this.x-x0)+(this.y-y0)*(this.y-y0));
      var vec = { x: (x0 - this.x)/dist, y: (y0 - this.y)/dist };
      // console.log(vec);
      var inX = (Math.abs(x0 - this.x) < this.stage.canvas.width / 2);
      var inY = (Math.abs(y0 - this.y) < this.stage.canvas.height / 2);
      this.fleche.x = this.stage.canvas.width / 2 + (this.stage.canvas.width / 2 - 100) * vec.x;
      this.fleche.y = this.stage.canvas.height / 2 + (this.stage.canvas.height / 2 - 100) * vec.y;
      this.fleche.rotation = Math.acos(vec.y) * 180 / Math.PI;
      if (x0 > this.x) {
        this.fleche.rotation = 360 - this.fleche.rotation;
      }
      // cacher fleche
      this.fleche.visible =  !inX || !inY;
    }
    if (this.energy == 0) {
      this.shape.gotoAndStop("go_in_house");
    }
    // redessin de la barre hsl(130,100%,50%)
    if (this.speed == this.initSpeed) {
      this.jaugeFillCommand.style = "hsl(" + (130 * this.energy/this.maxEnergy) + ", 100%, 50%)";
    }
    else {
      this.jaugeFillCommand.style = "red";
    }
    this.jauge.scaleX = (this.energy / this.maxEnergy);
    this.carriedItem.gotoAndStop(this.item);

  }

  computeNextCostume() {
    if (this.vec.x > 0.85) {
      return "go_right";
    }
    if (this.vec.x < -0.85) {
      return "go_left";
    }
    if (this.vec.y > 0) {
      return "go_down"
    }
    return "go_up";
  }


}
