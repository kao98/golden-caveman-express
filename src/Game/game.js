const Level = require('./level.js')
const Player = require('./player.js')

module.exports = class Game {
  constructor(loader, audio, debug) {
    this.loader = loader;
    this.audio = audio;
    this.debug = !!debug;

    this.canvas = null;
    this.started = false;
    this.paused = false;

    this.dev = false;
  }

  setCanvas(canvas) {
    this.canvas = canvas;

    this.canvas.width = this.viewWidth;
    this.canvas.height = this.viewHeight;

    this.stage = new createjs.Stage(this.canvas);
    this.stage.snapToPixel = true;

    this.player = new Player(this.loader, this.stage);
    this.level = new Level(this.loader, this.stage, this.player, this.audio);

    // this.setDevMode(true);
    this.addDebugData();
      
  }

  setDevMode(value) {
    this.level.home.dev = this.player.dev = this.dev = !!value;
    this.level.enemies.forEach((enemy) => enemy.dev = !!value);
    this.level.houses.forEach((house) => house.dev = !!value);
  }

  start() {
    this.paused = false;
    this.started = true;

    this.audio.playMainTheme();
  }

  pause() {
    this.paused = true;
    if (this.player.running)
      this.audio.stopMove();
  }

  resume() {
    this.paused = false;
    if (this.player.running)
      this.audio.playMove();
  }

  stop() {
    this.started = false;

  }

  setTicker() {
    createjs.Ticker.addEventListener("tick", (event) => this.tick(event));
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = 60;
  }

  handleKeydown(key) {
  }

  handleKeyup(key) {
    switch (key) {
      case ' ':
        if (this.level.player.running == false) {
          this.level.resumeGame();
        }
        break;
    }
  }

  handleMousedown(button, x, y, buttons) {
    this.player.increaseSpeed();
  }

  handleMouseup(button, x, y, buttons) {
    this.player.decreaseSpeed();
  }

  handleMousemove(x, y, buttons) {
    this.player.pointTo(x, y);
  }

  addDebugData() {
    if (!this.debug) {
      return;
    }

    const fontSize = 18;

    this.fpsLabel = new createjs.Text("-- fps", "bold " + fontSize + "px Arial", "#FF0");

    this.fpsLabel.x = 10;
    this.fpsLabel.y = 40;

    this.playerPosLabel = new createjs.Text("x, y", "normal " + fontSize + "px Arial", "#F0F");
    this.playerPosLabel.x = 10;
    this.playerPosLabel.y = 60;

    this.stage.addChild(this.fpsLabel);
    this.stage.addChild(this.playerPosLabel);
  }

  renderDebugData() {
    if (!this.debug) {
      return;
    }

    this.fpsLabel.text = Math.round(createjs.Ticker.getMeasuredFPS()) + " fps";
    this.playerPosLabel.text = Math.round(this.player.x) + ", " + Math.round(this.player.y);
  }

  tick(tickEvent) {
    if (!this.started || this.paused) {
      return;
    }

    this.level.update();
    this.stage.update();
    this.renderDebugData();
  }

  get viewWidth() {
    return 960;
  }

  get viewHeight() {
    return 540;
  }
}
