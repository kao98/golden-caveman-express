
module.exports = class Enemy {

  constructor(loader, stage, x, y) {
    this.x = x;
    this.y = y;
    this.vec = {x: 0, y: 0};
    this.stage = stage;

    this.speed = 4.5;

    this.startX = x;
    this.startY = y;

    this.dev = false;

    // image
    if (Math.random() < 0.5) {
        // cat --> saute sur le joueur si assez près
        this.type = 0;
        let sh = new createjs.SpriteSheet({
          images: [loader.get("cat")],
          frames: {count: 4, width: 32, height: 32 },
          animations: { idle: [0, 2, undefined, 0.24 ], attack: [3, 3] }
        });
        this.shape = new createjs.Sprite(sh, "idle");
        this.attackDistance = 100;
        this.speed = 10;
    }
    else {
      // demon --> poursuit le joueur jusqu'à une certaine limite
      this.type = 1;
      let sh = new createjs.SpriteSheet({
        images: [loader.get("pitit_demon")],
        frames: {count: 7, width: 32, height: 32},
        animations: { danse: [0, 6, undefined, 0.42 ] }
      });
      this.shape = new createjs.Sprite(sh, "danse");
      this.attackDistance = 200;
    }
    this.shape.scaleX = 1.6;
    this.shape.scaleY = 1.6;
    this.width = 32 * this.shape.scaleX;
    this.height = 32 * this.shape.scaleY;
    stage.addChild(this.shape);

    // collision shape
    this.collisionBox = new createjs.Shape();
    this.collisionBox.graphics.beginStroke("red");
    this.collisionBox.graphics.drawRect(this.width*0.2, this.height*0.1, this.width*0.6, this.height*0.8);
    this.collisionBox.graphics.endStroke();
    stage.addChild(this.collisionBox);
  }

  get bounds() {
    return { x: this.x + this.width * 0.2, y: this.y + this.height*0.1, w: this.width*0.6, h: this.height*0.8 };
  }

  update() {
    this.collisionBox.visible = this.dev;
    this.x += this.vec.x * this.speed;
    this.y += this.vec.y * this.speed;
  }

  isClosedTo(player) {
    return ((this.x-player.x)*(this.x-player.x)+(this.y-player.y)*(this.y-player.y)) < this.attackDistance*this.attackDistance;
  }

}
